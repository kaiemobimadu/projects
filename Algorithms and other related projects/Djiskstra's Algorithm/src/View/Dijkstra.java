package View;

/*
 * ideas taken from : https://github.com/alanwright/hackPack/blob/master/Graph by Alan Wright(2013)
 *          Optimized Dijkstra (Priority Queue)
 *            Runtime: O(E*log(v))
 *             Space: O(v^2)
*Author: Kasiemobi Maduabunachukwu
 */
import java.util.*;
import javax.swing.JOptionPane;

public class Dijkstra {

    public static final int NO_EDGE = -1;
    static node[] nodes = null;
    public static List<Integer> pathList = new ArrayList<>();

    public static void executePath(int start, int end) {

        //Create our graph again
        //node[] graphNodes = createGraphEdges(10,50);
        System.out.println("Running optimized Dijkstras from node 0");
        System.out.println("----------------------------------------");
        //Run dijkstras from 0
        node start2 = nodes[start];
        dijkstraPQ(nodes, start2);
//conditional statement for exception handling 
        for (node n : nodes) {
            if (n != start2 && n.index == end) {
                if (n.minWeight == Double.MAX_VALUE) {
                    JOptionPane.showMessageDialog(null, "NO PATH HERE");
                } else {
                    String a = "Cummulative Weight of Shortest Distance from " + start2.index + " to " + n.index + " is " + n.minWeight + "\n";
                    //a = a + ("\nWith path: ");
                    List<Integer> tempList = printShortestPathTo(n);
                    for (int i = 0; i < tempList.size(); i++) {
                        a = a + tempList.get(i) + " > ";
                        if (i % 10 == 0) {
                            a = a+"\n";
                        }
                    }
                    JOptionPane.showMessageDialog(null, a);
                }
            }
        }
    }

    //Creates the following bidirectional graph:
    // 
    /*        
	 
	
     */
    public static void createGraphSize(int size) {
        nodes = new node[size];
        for (int i = 0; i < size; i++) {
            nodes[i] = new node(i);
        }
    }

    //Creates the same graph as the createGraph function, but uses the node and edge
    // classes to be stored in a priority queue.
    //Edge re-copy is used because the degree of the graph is 2.8; efficiency not necessarily a concern here
    public static node[] createGraphEdges(int startNode, int endNode) {
        if (nodes[startNode].adj == null) {
            nodes[startNode].adj = new edge[]{new edge(nodes[endNode], computeWeight(startNode, endNode))};
        } else if (nodes[startNode].adj.length > 0) {
            //System.out.println("HERE >>>>>>>>>>>>>>>>>>>");
            edge[] currentEdge = nodes[startNode].adj;
            edge[] newEdgeArray = new edge[currentEdge.length + 1];
            System.arraycopy(currentEdge, 0, newEdgeArray, 0, currentEdge.length);
            newEdgeArray[currentEdge.length] = new edge(nodes[endNode], computeWeight(startNode, endNode));
            nodes[startNode].adj = newEdgeArray;
        }
        

        return nodes;
    }

    //This is the distance between two points
    public static double computeWeight(int nodeA, int nodeB) {
        String[] pathA = ReadPathData.adjacencyMapping.get("" + nodeA).split(":");
        String[] pathB = ReadPathData.adjacencyMapping.get("" + nodeB).split(":");
        int Ax = Integer.valueOf(pathA[0]);
        int Ay = Integer.valueOf(pathA[1]);
        int Bx = Integer.valueOf(pathB[0]);
        int By = Integer.valueOf(pathB[1]);
        return Math.sqrt(Math.pow(Math.abs(Ax - Bx), 2) + Math.pow(Math.abs(Ay - By), 2));
    }

    //Runs an optimized version of dijkstra with a priority queue that finds the shortest
    // distance from the start node, to all other nodes. 
    // Note: a very large number indicates the nodes are not connected. 
    public static void dijkstraPQ(node[] nodes, node source) {

        //Note that nodes store their minimum distances you would initialize here
        //Initialize our priority queue with the start node
        source.minWeight = 0;
        PriorityQueue<node> q = new PriorityQueue<>();
        q.add(source);

        while (!q.isEmpty()) {

            //Grab the top of the queue
            node curr = q.poll();
            //System.out.println("NODE: "+curr.index);
            //System.out.println("ADJACENCY: "+curr.adj);
            //Break if no minimum weight here
            if (curr.minWeight == Double.MAX_VALUE) {
                break;
            }

            //Look at each of the nodes edges
            if (curr.adj != null) {
                for (edge e : curr.adj) {

                    //Get the target of the edge
                    node v = e.end;

                    //Calculate the alternative weight of going through the new edge using double values
                    double alternative = curr.minWeight + e.weight;

                    //If this alternative weight is better
                    if (alternative < v.minWeight) {

                        //Update the priority queue
                        q.remove(v);
                        v.minWeight = alternative;
                        v.previous = curr;
                        q.add(v);
                    }
                }
            }
        }
    }

    //Recursively bubbles backwards building the shortest path
    // and printing it. 
    public static List<Integer> printShortestPathTo(node end) {
        List<node> path = new ArrayList<>();
        List<Integer> pathNodes = new ArrayList<>();
        for (node n = end; n != null; n = n.previous) {
            path.add(n);
        }
        Collections.reverse(path);

        int sum = 0;
        for (node n : path) {
            pathNodes.add(n.index);
            System.out.print(n.index + " -> ");
        }
        //System.out.println("done");
        pathList = pathNodes;
        return pathNodes;
    }
}

//node class that keeps track of a nodes shortest path from the source
// and all it's edges. Sorted according to weight for the priority queue. 
class node implements Comparable<node> {

    public int index;
    public edge[] adj;
    public double minWeight = Double.MAX_VALUE;
    public node previous;

    public node(int i) {
        index = i;
    }

    public int compareTo(node other) {
        double comparison = minWeight - other.minWeight;
        int retValue = 0;
        if (comparison > 0.0) {
            retValue = 1;
        } else if (comparison < 0.0) {
            retValue = -1;
        }
        return retValue;
    }
}

//edge class stores the weight and destination of an edge. 
class edge {

    public node end;
    public double weight;

    public edge(node e, double w) {
        end = e;
        weight = w;
    }
}
