/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author kasiemobi Maduabunachukwu
 */
public class ReadPathData {
    //Holds map x:y co-ordinates
    public static List<String> pathList = new ArrayList<>();
    public static List<String> pathNetwork = new ArrayList<>();
    public static Map<String,String> adjacencyMapping = new HashMap<>();
    public static Map<String,String> adjacencyMappingReverse = new HashMap<>();
    public static final Pattern VALID_COORDINATE_INPUT_REGEX = Pattern.compile("^[_0-9]*:+[_0-9]*$");
    public static final Pattern VALID_INDEX_INPUT_REGEX = Pattern.compile("[0-9]+");
    public static String source = "-1",target = "-1";
    
    public static void readFile(String path) throws FileNotFoundException{
        String regex = " " + "+";
        Pattern scriptPattern = Pattern.compile(regex);
        Matcher matcher = null;
        Scanner scanner = new Scanner(new FileReader(path));
        if(scanner.hasNextLine()){
            matcher = scriptPattern.matcher(scanner.nextLine().trim());
            //create size of graph here. This is the first item in the file
            String[] nodeAndEdgeSizes = matcher.replaceAll(" ").split(" ");
            Dijkstra.createGraphSize(Integer.valueOf(nodeAndEdgeSizes[0]));
        }
        while(scanner.hasNextLine()){
            matcher = scriptPattern.matcher(scanner.nextLine().trim());
            pathList.add(matcher.replaceAll(" "));
        }
        System.out.println("Done reading files");
    }
    //returns -1 if bad format of if item does not exists, else returns the node index
    public static String isRequiredInputFormat(String input){
        String retValue = "-1";
        String coord = convertCoordinateToIndex(input);
        if(coord != null){
            //check if coordinate exists
            retValue = coord;
            
        }else{
                if(adjacencyMapping.containsKey(input)){
                    retValue = input;
                }
        }
        if(retValue.equals("-1")){
            JOptionPane.showMessageDialog(null, "Wrong Input Format ("+input+")!\nEnter Valid Input \nCo-ordinate = 6345:2354\nOR\nNode Index: 245");
        }
                
        return retValue;
    }
    
    public static String convertCoordinateToIndex(String input){
        String retVal = null;
        if(adjacencyMappingReverse.containsKey(input)){
            retVal = adjacencyMappingReverse.get(input);
        }
        return retVal;
    }
    

    
}
