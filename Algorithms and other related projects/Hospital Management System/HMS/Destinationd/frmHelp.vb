Public Class frmHelp
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txt_AddRecords As System.Windows.Forms.TextBox
    Friend WithEvents txt_print As System.Windows.Forms.TextBox
    Friend WithEvents txt_Search As System.Windows.Forms.TextBox
    Friend WithEvents txt_Export As System.Windows.Forms.TextBox
    Friend WithEvents txt_Email As System.Windows.Forms.TextBox
    Friend WithEvents txt_Calculator As System.Windows.Forms.TextBox
    Friend WithEvents txt_Notepad As System.Windows.Forms.TextBox
    Friend WithEvents GB_HelpIndex As System.Windows.Forms.GroupBox
    Friend WithEvents ll_Notepad As System.Windows.Forms.LinkLabel
    Friend WithEvents ll_Calculator As System.Windows.Forms.LinkLabel
    Friend WithEvents ll_Email As System.Windows.Forms.LinkLabel
    Friend WithEvents ll_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents ll_Search As System.Windows.Forms.LinkLabel
    Friend WithEvents ll_Print As System.Windows.Forms.LinkLabel
    Friend WithEvents ll_AddRecords As System.Windows.Forms.LinkLabel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHelp))
        Me.txt_AddRecords = New System.Windows.Forms.TextBox
        Me.txt_print = New System.Windows.Forms.TextBox
        Me.txt_Search = New System.Windows.Forms.TextBox
        Me.txt_Export = New System.Windows.Forms.TextBox
        Me.txt_Email = New System.Windows.Forms.TextBox
        Me.txt_Calculator = New System.Windows.Forms.TextBox
        Me.txt_Notepad = New System.Windows.Forms.TextBox
        Me.GB_HelpIndex = New System.Windows.Forms.GroupBox
        Me.ll_Notepad = New System.Windows.Forms.LinkLabel
        Me.ll_Calculator = New System.Windows.Forms.LinkLabel
        Me.ll_Email = New System.Windows.Forms.LinkLabel
        Me.ll_Export = New System.Windows.Forms.LinkLabel
        Me.ll_Search = New System.Windows.Forms.LinkLabel
        Me.ll_Print = New System.Windows.Forms.LinkLabel
        Me.ll_AddRecords = New System.Windows.Forms.LinkLabel
        Me.GB_HelpIndex.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_AddRecords
        '
        Me.txt_AddRecords.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_AddRecords.Location = New System.Drawing.Point(24, 32)
        Me.txt_AddRecords.Multiline = True
        Me.txt_AddRecords.Name = "txt_AddRecords"
        Me.txt_AddRecords.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txt_AddRecords.Size = New System.Drawing.Size(312, 417)
        Me.txt_AddRecords.TabIndex = 9
        Me.txt_AddRecords.Text = resources.GetString("txt_AddRecords.Text")
        '
        'txt_print
        '
        Me.txt_print.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_print.Location = New System.Drawing.Point(443, 503)
        Me.txt_print.Multiline = True
        Me.txt_print.Name = "txt_print"
        Me.txt_print.Size = New System.Drawing.Size(280, 32)
        Me.txt_print.TabIndex = 17
        Me.txt_print.Text = "1. List         : Prints the list of record." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2. Current  : Print current or pa" & _
            "rticular        record through search form."
        '
        'txt_Search
        '
        Me.txt_Search.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Search.Location = New System.Drawing.Point(443, 455)
        Me.txt_Search.Multiline = True
        Me.txt_Search.Name = "txt_Search"
        Me.txt_Search.Size = New System.Drawing.Size(280, 40)
        Me.txt_Search.TabIndex = 18
        Me.txt_Search.Text = resources.GetString("txt_Search.Text")
        '
        'txt_Export
        '
        Me.txt_Export.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Export.Location = New System.Drawing.Point(40, 543)
        Me.txt_Export.Multiline = True
        Me.txt_Export.Name = "txt_Export"
        Me.txt_Export.Size = New System.Drawing.Size(280, 40)
        Me.txt_Export.TabIndex = 19
        Me.txt_Export.Text = "By using this functionality you can export the list of patients , doctors , nurse" & _
            " or wardboy, discharge , bill  to excel."
        '
        'txt_Email
        '
        Me.txt_Email.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Email.Location = New System.Drawing.Point(40, 495)
        Me.txt_Email.Multiline = True
        Me.txt_Email.Name = "txt_Email"
        Me.txt_Email.Size = New System.Drawing.Size(280, 40)
        Me.txt_Email.TabIndex = 20
        Me.txt_Email.Text = resources.GetString("txt_Email.Text")
        '
        'txt_Calculator
        '
        Me.txt_Calculator.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Calculator.Location = New System.Drawing.Point(443, 543)
        Me.txt_Calculator.Multiline = True
        Me.txt_Calculator.Name = "txt_Calculator"
        Me.txt_Calculator.Size = New System.Drawing.Size(280, 24)
        Me.txt_Calculator.TabIndex = 21
        Me.txt_Calculator.Text = "It helps in calculations like calculating the the total amount of patient's bill " & _
            "etc."
        '
        'txt_Notepad
        '
        Me.txt_Notepad.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Notepad.Location = New System.Drawing.Point(443, 575)
        Me.txt_Notepad.Multiline = True
        Me.txt_Notepad.Name = "txt_Notepad"
        Me.txt_Notepad.Size = New System.Drawing.Size(280, 24)
        Me.txt_Notepad.TabIndex = 22
        Me.txt_Notepad.Text = "It helps to write notes."
        '
        'GB_HelpIndex
        '
        Me.GB_HelpIndex.BackColor = System.Drawing.Color.Thistle
        Me.GB_HelpIndex.Controls.Add(Me.ll_Notepad)
        Me.GB_HelpIndex.Controls.Add(Me.ll_Calculator)
        Me.GB_HelpIndex.Controls.Add(Me.ll_Email)
        Me.GB_HelpIndex.Controls.Add(Me.ll_Export)
        Me.GB_HelpIndex.Controls.Add(Me.ll_Search)
        Me.GB_HelpIndex.Controls.Add(Me.ll_Print)
        Me.GB_HelpIndex.Controls.Add(Me.ll_AddRecords)
        Me.GB_HelpIndex.Location = New System.Drawing.Point(499, 58)
        Me.GB_HelpIndex.Name = "GB_HelpIndex"
        Me.GB_HelpIndex.Size = New System.Drawing.Size(168, 347)
        Me.GB_HelpIndex.TabIndex = 23
        Me.GB_HelpIndex.TabStop = False
        Me.GB_HelpIndex.Text = "Index"
        '
        'll_Notepad
        '
        Me.ll_Notepad.Location = New System.Drawing.Point(40, 287)
        Me.ll_Notepad.Name = "ll_Notepad"
        Me.ll_Notepad.Size = New System.Drawing.Size(88, 24)
        Me.ll_Notepad.TabIndex = 15
        Me.ll_Notepad.TabStop = True
        Me.ll_Notepad.Text = "Notepad"
        '
        'll_Calculator
        '
        Me.ll_Calculator.Location = New System.Drawing.Point(40, 243)
        Me.ll_Calculator.Name = "ll_Calculator"
        Me.ll_Calculator.Size = New System.Drawing.Size(88, 24)
        Me.ll_Calculator.TabIndex = 14
        Me.ll_Calculator.TabStop = True
        Me.ll_Calculator.Text = "Calculator"
        '
        'll_Email
        '
        Me.ll_Email.Location = New System.Drawing.Point(40, 199)
        Me.ll_Email.Name = "ll_Email"
        Me.ll_Email.Size = New System.Drawing.Size(88, 24)
        Me.ll_Email.TabIndex = 13
        Me.ll_Email.TabStop = True
        Me.ll_Email.Text = "Email"
        '
        'll_Export
        '
        Me.ll_Export.Location = New System.Drawing.Point(40, 155)
        Me.ll_Export.Name = "ll_Export"
        Me.ll_Export.Size = New System.Drawing.Size(88, 24)
        Me.ll_Export.TabIndex = 12
        Me.ll_Export.TabStop = True
        Me.ll_Export.Text = "Export"
        '
        'll_Search
        '
        Me.ll_Search.Location = New System.Drawing.Point(40, 116)
        Me.ll_Search.Name = "ll_Search"
        Me.ll_Search.Size = New System.Drawing.Size(88, 24)
        Me.ll_Search.TabIndex = 11
        Me.ll_Search.TabStop = True
        Me.ll_Search.Text = "Search"
        '
        'll_Print
        '
        Me.ll_Print.Location = New System.Drawing.Point(40, 76)
        Me.ll_Print.Name = "ll_Print"
        Me.ll_Print.Size = New System.Drawing.Size(88, 16)
        Me.ll_Print.TabIndex = 10
        Me.ll_Print.TabStop = True
        Me.ll_Print.Text = "Print"
        '
        'll_AddRecords
        '
        Me.ll_AddRecords.Location = New System.Drawing.Point(40, 32)
        Me.ll_AddRecords.Name = "ll_AddRecords"
        Me.ll_AddRecords.Size = New System.Drawing.Size(88, 16)
        Me.ll_AddRecords.TabIndex = 9
        Me.ll_AddRecords.TabStop = True
        Me.ll_AddRecords.Text = "Add Record"
        '
        'frmHelp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(7, 19)
        Me.BackColor = System.Drawing.Color.LightSlateGray
        Me.ClientSize = New System.Drawing.Size(794, 630)
        Me.Controls.Add(Me.GB_HelpIndex)
        Me.Controls.Add(Me.txt_Notepad)
        Me.Controls.Add(Me.txt_Calculator)
        Me.Controls.Add(Me.txt_Email)
        Me.Controls.Add(Me.txt_Export)
        Me.Controls.Add(Me.txt_Search)
        Me.Controls.Add(Me.txt_print)
        Me.Controls.Add(Me.txt_AddRecords)
        Me.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHelp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Help"
        Me.GB_HelpIndex.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region


#Region "Form load event"

    Private Sub frmHelp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txt_AddRecords.Visible = True
        txt_print.Visible = False
        txt_Search.Visible = False
        txt_Export.Visible = False
        txt_Email.Visible = False
        txt_Calculator.Visible = False
        txt_Notepad.Visible = False
    End Sub

#End Region

#Region "All link label events"

    Private Sub ll_AddRecords_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles ll_AddRecords.LinkClicked
        txt_AddRecords.Visible = True
        txt_print.Visible = False
        txt_Search.Visible = False
        txt_Export.Visible = False
        txt_Email.Visible = False
        txt_Calculator.Visible = False
        txt_Notepad.Visible = False
    End Sub

    Private Sub ll_Print_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles ll_Print.LinkClicked
        txt_AddRecords.Visible = False
        txt_print.Visible = True
        txt_Search.Visible = False
        txt_Export.Visible = False
        txt_Email.Visible = False
        txt_Calculator.Visible = False
        txt_Notepad.Visible = False

        txt_print.ScrollBars = ScrollBars.Both
        txt_print.Location = New Point(24, 32)
        txt_print.Size = New Size(296, 384)
    End Sub

    Private Sub ll_Search_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles ll_Search.LinkClicked
        txt_AddRecords.Visible = False
        txt_print.Visible = False
        txt_Search.Visible = True
        txt_Export.Visible = False
        txt_Email.Visible = False
        txt_Calculator.Visible = False
        txt_Notepad.Visible = False

        txt_Search.ScrollBars = ScrollBars.Both
        txt_Search.Location = New Point(24, 32)
        txt_Search.Size = New Size(296, 384)
    End Sub

    Private Sub ll_Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles ll_Export.LinkClicked
        txt_AddRecords.Visible = False
        txt_print.Visible = False
        txt_Search.Visible = False
        txt_Export.Visible = True
        txt_Email.Visible = False
        txt_Calculator.Visible = False
        txt_Notepad.Visible = False

        txt_Export.ScrollBars = ScrollBars.Both
        txt_Export.Location = New Point(24, 32)
        txt_Export.Size = New Size(296, 384)
    End Sub

    Private Sub ll_Email_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles ll_Email.LinkClicked
        txt_AddRecords.Visible = False
        txt_print.Visible = False
        txt_Search.Visible = False
        txt_Export.Visible = False
        txt_Email.Visible = True
        txt_Calculator.Visible = False
        txt_Notepad.Visible = False

        txt_Email.ScrollBars = ScrollBars.Both
        txt_Email.Location = New Point(24, 32)
        txt_Email.Size = New Size(296, 384)
    End Sub

    Private Sub ll_Calculator_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles ll_Calculator.LinkClicked
        txt_AddRecords.Visible = False
        txt_print.Visible = False
        txt_Search.Visible = False
        txt_Export.Visible = False
        txt_Email.Visible = False
        txt_Calculator.Visible = True
        txt_Notepad.Visible = False

        txt_Calculator.ScrollBars = ScrollBars.Both
        txt_Calculator.Location = New Point(24, 32)
        txt_Calculator.Size = New Size(296, 384)
    End Sub

    Private Sub ll_Notepad_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles ll_Notepad.LinkClicked
        txt_AddRecords.Visible = False
        txt_print.Visible = False
        txt_Search.Visible = False
        txt_Export.Visible = False
        txt_Email.Visible = False
        txt_Calculator.Visible = False
        txt_Notepad.Visible = True

        txt_Notepad.ScrollBars = ScrollBars.Both
        txt_Notepad.Location = New Point(24, 32)
        txt_Notepad.Size = New Size(296, 384)
    End Sub

#End Region

#Region "All link label key event"

    Private Sub txt_AddRecords_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_AddRecords.KeyPress
        If Char.IsLetterOrDigit(e.KeyChar) Or Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar) = False Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Or e.KeyChar = CChar(ChrW(Keys.Space)) Then
                e.Handled = True
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txt_print_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_print.KeyPress
        If Char.IsLetterOrDigit(e.KeyChar) Or Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar) = False Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Or e.KeyChar = CChar(ChrW(Keys.Space)) Then
                e.Handled = True
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txt_Search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Search.KeyPress
        If Char.IsLetterOrDigit(e.KeyChar) Or Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar) = False Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Or e.KeyChar = CChar(ChrW(Keys.Space)) Then
                e.Handled = True
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txt_Export_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Export.KeyPress
        If Char.IsLetterOrDigit(e.KeyChar) Or Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar) = False Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Or e.KeyChar = CChar(ChrW(Keys.Space)) Then
                e.Handled = True
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txt_Email_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Email.KeyPress
        If Char.IsLetterOrDigit(e.KeyChar) Or Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar) = False Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Or e.KeyChar = CChar(ChrW(Keys.Space)) Then
                e.Handled = True
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txt_Calculator_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Calculator.KeyPress
        If Char.IsLetterOrDigit(e.KeyChar) Or Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar) = False Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Or e.KeyChar = CChar(ChrW(Keys.Space)) Then
                e.Handled = True
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txt_Notepad_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_Notepad.KeyPress
        If Char.IsLetterOrDigit(e.KeyChar) Or Char.IsNumber(e.KeyChar) Or Char.IsLetter(e.KeyChar) = False Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Or e.KeyChar = CChar(ChrW(Keys.Space)) Then
                e.Handled = True
            Else
                e.Handled = True
            End If
        End If
    End Sub
#End Region

End Class
