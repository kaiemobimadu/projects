package pythagorean.triple.alogrithm;
/*
   //This class is used to generate pythagorean values 
   //for the variable x1, y1, z1, x2, y2, z2, x3, y3, z3
Author: Kasiemobi Maduabunachukwu
*/
public class AlgorithmFormulas {
    int p,q; //the values for p and q are set here
    
    int x1,x2,x3; //the pythagoraen triple for the x value 
    int y1,y2,y3; //the pythagoraen triple for the y value 
    int z1,z2,z3; //the pythagoraen triple for the z value
    
    public AlgorithmFormulas(int p1, int q1) {
        //This constructor initializes the value for p and q
        p = p1; //p is set to p1's value
        q = q1; //q is set to q1's value
    }
    
    public int tripleAlgorithmX1() {
        x1 = (2*p*p) + (2*p*q); //the formular for x1
        x1 = x1 % 26; //the modulus of x1
        if (x1 < 0) { //checks if the modulus x1 is less than 0
            x1 = 26 - ((-1)*x1);
        }
        return x1; //returns the result x1
    }
    
    public int tripleAlgorithmX2() {
        x2 = (2*p*p) - (2*p*q); //the formular for x2
        x2 = x2 % 26; //the modulus of x2
        if (x2 < 0) { //checks if the modulus x2 is less than 0
            x2 = 26 - ((-1)*x2);
        }
        return x2; //returns the result x2
    }
    
    public int tripleAlgorithmX3() { 
        x3 = 2*p*q; //the formular for x3
        x3 = x3 % 26; //the modulus of x3
        if (x3 < 0) { //checks if the modulus x3 is less than 0
            x3 = 26 - ((-1)*x3);
        }
        return x3; //returns the result x3
    }
    
    public int tripleAlgorithmY1() {
        y1 = (q*q) + (2*p*q); //the formular for y1
        y1 = y1 % 26; //the modulus of y1
        if (y1 < 0) { //checks if the modulus y1 is less than 0
            y1 = 26 - ((-1)*y1);
        }
        return y1; //returns the result y1
    }
    
    public int tripleAlgorithmY2() {
        y2 = (q*q) - (2*p*q); //the formular for y2
        //System.out.println(y2); //the modulus of y2
        if (y2 < 0) { //checks if the modulus y2 is less than 0
            y2 = 26 - ((-1)*y2);
        }
        return y2; //returns the result y2
    }
    
    public int tripleAlgorithmY3() {
        y3 = (p*p) - (q*q); //the formular for y3
        y3 = y3 % 26; //the modulus of y3
        if (y3 < 0) { //checks if the modulus y3 is less than 0
            y3 = 26 - ((-1)*y3);
        }
        return y3; //returns the result y3
    }
    
    public int tripleAlgorithmZ1() {
        z1 = (2*p*p) + (q*q) + (2*p*q); //the formular for z1
        z1 = z1 % 26; //the modulus of z1
        if (z1 < 0) { //checks if the modulus z1 is less than 0
            z1 = 26 - ((-1)*z1);
        }
        return z1; //returns the result z1
    }
    
    public int tripleAlgorithmZ2() {
        z2 = (2*p*p) + (q*q) - (2*p*q); //the formular for z2
        z2 = z2 % 26; //the modulus of z2
        if (z2 < 0) { //checks if the modulus z2 is less than 0
            z2 = 26 - ((-1)*z2);
        }
        return z2; //returns the result z2
    }
    
    public int tripleAlgorithmZ3() {
        z3 = (p*p) + (q*q);//the formular for z3
        z3 = z3 % 26; //the modulus of z3
        if (y2 < 0) { //checks if the modulus z3 is less than 0
            z3 = 26 - ((-1)*z3);
        }
        return z3; //returns the result z3
    }
}
