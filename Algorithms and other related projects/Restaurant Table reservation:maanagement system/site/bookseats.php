<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable session_username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['session_username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['session_username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<html>
<head>
<META HTTP-EQUIV="refresh" CONTENT="5;URL=mtn/">
	<title>Table Reservation</title>
		<style>
		* {
			font-size: 11px;
			font-family: arial;
		}
		.scrollable {

	/* required settings */
	position:relative;
	overflow:hidden;
	width: 600px;
	height:600px;
	border:10px solid #FFFFFF;
	background-color:#d0cfcb;}
	
	
/*
	a single item. must be floated in horizontal scrolling.
	typically, this element is the one that *you* will style
	the most.
*/
.items div {
	float:left;
	padding:5px;
	color:#FFF;
}

.tool_button {
	height:24px;
	width:24px; 
	padding:5px; 
	cursor:pointer;

}
}
	</style>
    <script type='text/javascript' src='../jquery/jquery-1.4.2.min.js'></script>
<script type='text/javascript' src='../jquery/plugins/jquery.tools.min.js'></script>
<script type='text/javascript' src='../js/js.functions.common.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		$(".scrollable").scrollable();
	});
	</script>
</head>
<body>
<div class="scrollable">   
   <div style="background-image:url(images/row_tool_bg.png); background-repeat:repeat-x; width:100%;text-align:right;">
	<a class="prev browse left"><img src="images/left_32.png" class="tool_button" title="Back" onMouseOver="this.style.backgroundColor='#000000'" onMouseOut="this.style.backgroundColor=''" /></a>
    <a class="next browse right"><img src="images/right_32.png" class="tool_button" onMouseOver="this.style.backgroundColor='#000000'" onMouseOut="this.style.backgroundColor=''" title="Next"/></a>
    <img src="images/delete_32.png" class="tool_button" onMouseOver="this.style.backgroundColor='#000000'" onMouseOut="this.style.backgroundColor=''" title="Close" onClick="self.parent.Shadowbox.close();"/>
  </div>
  <div class="items">
<center>
<br/>
<br/>
<br/>
<?php
echo "<center><h3>You are logged in as " . $_SESSION['session_username'] . "</h3></center>";
	if (isset($_POST['seats']))
	{
		$user = $_SESSION['session_username'];
	
		$newStatusCode = $_POST['newStatusCode'];
		$oldStatusCode = $_POST['oldStatusCode'];

		// open database connection
		$linkID = @ mysql_connect("localhost", "root", '') or die("Could not connect to MySQL server");
		@ mysql_select_db("tickets") or die("Could not select database");

		// prepare select statement
		$selectQuery = "SELECT rowId, columnId from seats where (";
		$count = 0;
		foreach($_POST['seats'] AS $seat) {
			if ($count > 0) {
				$selectQuery .= " || ";
			}
			$selectQuery .= " ( rowId = '" . substr($seat, 0, 1) . "'";
			$selectQuery .= " and columnId = " . substr($seat, 1) . " ) ";
			$count++;
		}
		$selectQuery .= " ) and status = $oldStatusCode";
		if ($oldStatusCode == 1) {
			$selectQuery .= " and updatedby = '$user'";
		}
		
		//echo $selectQuery;
		
		// execute select statement
		$result = mysql_query($selectQuery);
		//echo $result;
		
		$selectedSeats = mysql_num_rows($result);
		//echo "<br/>" . $selectedSeats;
		
		if ($selectedSeats != $count) {
			$problem = "<h3>There was a problem executing your request. No seat/s were updated.</h3>";
			$problem .= "Possible problems are:";
			$problem .= "<ul>";
			$problem .= "<li>Another process was able to book the same seat while you were still browsing.</li>";
			$problem .= "<li>You were trying to Confirm an unreserved Seat.</li>";
			$problem .= "<li>You were trying to Cancel an unreserved Seat.</li>";
			$problem .= "<li>You were trying to Reserve a reserved Seat.</li>";
			$problem .= "<li>There was a problem connecting to the database.</li>";
			$problem .= "</ul>";
			$problem .= "<a href='seats.php'>View Seat Plan</a>";
			die ($problem);
		}
		
		// prepare update statement
		$newStatusCode = $_POST['newStatusCode'];
		$oldStatusCode = $_POST['oldStatusCode'];
		
		$updateQuery = "UPDATE seats set status=$newStatusCode, updatedby='$user' where ( ";
		$count = 0;
		foreach($_POST['seats'] AS $seat) {
			if ($count > 0) {
				$updateQuery .= " || ";
			}
			$updateQuery .= " ( rowId = '" . substr($seat, 0, 1) . "'";
			$updateQuery .= " and columnId = " . substr($seat, 1) . " ) ";
			$count++;
		}
		$updateQuery .= " ) and status = $oldStatusCode";
		if ($oldStatusCode == 1) {
			$updateQuery .= " and updatedby = '$user'";
		}
		
		// perform update
		$result = mysql_query($updateQuery);
		$updatedSeats = mysql_affected_rows();
		//do Reservation
		foreach($_POST['seats'] AS $seat) {
			$reserve = mysql_query("INSERT INTO reservation(reservation_table,reservation_guest_name) VALUES('$seat','$user')");
			}
		

		if ($result && $updatedSeats == $count) {
			//$mysql->commit();
			echo "<h3>";
			echo "You have successfully updated $updatedSeats seat/s: ";
			echo "[";
			foreach($_POST['seats'] AS $seat) {
				$rowId = substr($seat, 0, 1);
				$columnId = substr($seat, 1);
				echo $rowId . $columnId . ", ";	
			}
			echo "]";
			echo "...</h3>";
		} else {
			//$mysql->rollback();
			echo "<h3>There was a problem executing your request. No seat/s were updated.</h3>";
			echo "Possible problems are:";
			echo "<ul>";
			echo "<li>Another process was able to book the same seat while you were still browsing.</li>";
			echo "<li>You were trying to Confirm an unreserved Seat.</li>";
			echo "<li>You were trying to Cancel an unreserved Seat.</li>";
			echo "<li>You were trying to Reserve a reserved Seat.</li>";
			echo "<li>There was a problem connecting to the database.</li>";
			echo "</ul>";
		}
		
		echo "<a href='seats.php'>View Seat Plan</a>";
		
		// Enable the autocommit feature
		//$mysqldb->autocommit(TRUE);
		
		// Recuperate the query resources
		//$result->free();
		
		mysql_close();
	}
?>

</center>
</div>
</body>
</html>