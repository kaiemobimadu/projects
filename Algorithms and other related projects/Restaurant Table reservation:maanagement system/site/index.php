<?php session_start();
	if(!isset($_SESSION['session_username'])) {
		header ("location: login.php");
	} 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Frankies | <?php echo $_SESSION['session_username']; ?> |</title>
<link rel="stylesheet" type="text/css" href="shadowbox-custom-3.0rc1/shadowbox.css">
<script type="text/javascript" src="shadowbox-custom-3.0rc1/shadowbox.js"></script>
<script src="shadowbox-custom-3.0rc1/shadowbox-iframe.js"></script>
<script src="shadowbox-custom-3.0rc1/shadowbox-base.js"></script>
<script type="text/javascript">Shadowbox.init({displayNav:true,animate:true,animateFade:false,overlayOpacity:0,modal:true});</script>

<script type='text/javascript' src='jquery/jquery-1.4.2.min.js'></script>
<script type='text/javascript' src='jquery/plugins/jquery.jqDock.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		   // set up the options to be used for jqDock...
	  var dockOptions =
		{ 
			
			size:120 ,
		    labels:true
		};
	  // ...and apply...
	  $('#menu').jqDock(dockOptions);

	});
	
	/*-------------------------- Shadow Box ----------------------*/
	function shadowBoxLoad(url,w,h,t,p) {
		Shadowbox.open({
			content:    url,
			player:     p,
			title:      t,
			height:     h,
			width:      w
		});
	}
	
</script>
<style type="text/css">
	div.jqDockLabel { white-space:nowrap; color:#ffffff; font:15px verdana; font-weight:bold; text-align:center;}
	#menu img {padding:0 10px;}
	#menu div.jqDockWrap {margin:0 auto;}
	#menu {position:absolute; bottom:20px; left:0; width:100%; display:none;}



</style>
</head>

<body style="margin:0px; padding:0px; background:url(images/back.png) repeat-x scroll left top #292929;">
<div>
<div id='menu'>
	<a onclick="javascript:shadowBoxLoad('profile.php',680,520,'','iframe');" style="cursor:pointer;">
		<img src='images/profile.png' alt="Profile" title="Profile"/>
    </a>
	<a onclick="javascript:shadowBoxLoad('seats.php',680,520,'','iframe');" style="cursor:pointer;">
		<img src='images/quiz.png' alt='Take Quiz' title='Book Seats'/>
	</a>


    <a href="logout.php" style="cursor:pointer;">
		<img src='images/logout.png' alt='Logout' title='Logout'/>
	</a>
</div>
</div>
</body>
</html>

