<?php session_start();
	error_reporting(0);
	if(isset($_SESSION['session_username'])) {
		header ("location: index.php");
	} 
	require_once("includes/includes.db.php");
	require_once("xajax_core/xajax.inc.php");
	//require_once("includes/includes.functions.php");	
	
function loginUser($formValues) {
		$objResponse = new xajaxResponse();
			if(trim($formValues['username']) != "" && trim($formValues['password']) != "") {
				if(trim($formValues['username']) == "admin" && trim($formValues['password']) == "admin" ) {
					$_SESSION['session_username'] = "admin";
					$_SESSION['session_name']     = "Administrator";
										
					$objResponse->redirect( "index.php" );
				} else {
					$q = "SELECT commonname,username FROM userauth WHERE username='".trim($formValues['username'])."' AND pswd='".trim($formValues['password'])."'";
					$r = mysql_query($q) or die(mysql_error());  
					$row = mysql_fetch_assoc($r);	
					if($row > 0) {
						if($row['username'] != '') {
							$_SESSION['session_username'] = $row['username'];
							$_SERVER['PHP_AUTH_USER'] = $_SESSION['session_username'];
							
							$objResponse->redirect( "index.php" );
						
						} 
					} else $objResponse->script("alert('Invalid Login!')");
				}
			} else $objResponse->script("alert('All Fields Required!')"); 
				
		return $objResponse;
	}

	
	$xajax = new xajax();

	$xajax->registerFunction("loginUser");
	$xajax->processRequest();
	
?>	



<!--
	Copyright 2009 Jeremie Tisseau
	"Sliding Login Panel with jQuery 1.3.2" is distributed under the GNU General Public License version 3:
	http://www.gnu.org/licenses/gpl-3.0.html
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<?php $xajax->printJavascript();?>
  	<title>Table Reservation System</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	

	<link type="text/css" href="jquery/css/south-street/jquery-ui-1.8.custom.css" rel="stylesheet" />	
	 
    <!-- jQuery - the core -->
	<script type='text/javascript' src='jquery/jquery-1.4.2.min.js'></script>    

	<script type='text/javascript' src='js/js.functions.common.js'></script>
    
	<script language="javascript">
        function submitUserLogin() {
            if(validateFilledForm(document.loginForm)) {
                xajax_loginUser(xajax.getFormValues("loginForm"));
            } else alert("All fields required!");
            return false;
        }
    </script>
		<style type="text/css">
			/*demo page css*/
			body{font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;}
		
</style>
</head>

<body style="padding:0px; margin:0px; background:url(images/default_bg.jpg) repeat-x scroll left top #292929;">
	<center>
	<div  class="ui-widget-content ui-corner-all" style="width:250px; height:200px; margin-top:100px;">
		
		<!-- Login Form -->
		<form  name="loginForm" id="loginForm"  action="javascript:void(null);" onsubmit="submitUserLogin();">
			<div class="ui-state-default  ui-corner-all" style="position:relative; top:0px; left:0px; padding:5px;"> Table Reservation Login</div>
			<div style="position:relative; top:10px; left:22px; text-align:left;">Username:</div>
			<div style="position:relative; top:15px; left:0px; text-align:center;"><input  class="ui-widget-content" type="text" name="username" id="username" value="" style="width:200px; padding:5px; font-size:12px; height:25px;" /></div>
			<div style="position:relative; top:20px; left:22px; text-align:left;">Password:</div>
			<div style="position:relative; top:25px; left:0px; text-align:center;"><input  class="ui-widget-content" type="password" name="password" id="password" style="width:200px; padding:5px; font-size:12px; height:25px;" /></div>
			<div style="position:relative; top:40px; left:0px; text-align:center;"><input type="submit" name="submit" value="Login" class="ui-state-default ui-corner-all" style="padding:5px; width:100px;"/><a href="register.php" class="ui-state-default ui-corner-all" style="padding:5px; width:100px;">Register</a></div>
		</form>
		
      <p>&nbsp;</p>
</div>
	<div class="ui-accordion"></div>
    </center>
</body>
</html>
