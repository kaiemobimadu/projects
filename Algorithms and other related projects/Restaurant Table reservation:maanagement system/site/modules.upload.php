<?php session_start();
	error_reporting(0);
	if(isset($_SESSION['session_user_id'])) {
		header ("location: index.php");
	} 
	
	require_once("includes/includes.db.php");

//Check if submit button exist and click
if(isset($_POST['xsubmit'])) {
  // get the original filename
  $image = $_FILES['upload']['name'];
 
  // image storing folder, make sure you indicate the right path
  $folder = "../upload/"; 
 
  // image checking if exist or the input field is not empty
  if($image) { 
    // creating a filename
    $filename = $folder . $image; 
	
	$q = mysql_query("UPDATE userauth set image = '$image' WHERE username = '$_SESSION[session_username]'");
 
    // uploading image file to specified folder
    $copied = copy($_FILES['upload']['tmp_name'], $filename); 
 
    // checking if upload succesfull
    if (!$copied) { 
 
      // creating variable for the purpose of checking: 
      // 0-unsuccessfull, 1-successfull
      $ok = 0; 
    } else {
      $ok = 1;
    }
  }
}
?>


<html>
<head>
<style type="text/css">
/*
	root element for the scrollable.
	when scrolling occurs this element stays still.
*/
.scrollable {

	/* required settings */
	position:relative;
	overflow:hidden;
	width: 660px;
	height:500px;
	border:10px solid #FFFFFF;
	background-color:#d0cfcb;
}

/*
	root element for scrollable items. Must be absolutely positioned
	and it should have a extremely large width to accommodate scrollable items.
	it's enough that you set width and height for the root element and
	not for this element.
*/
.scrollable .items {
	/* this cannot be too large */
	width:20000em;
	position:absolute;
	


}

/*
	a single item. must be floated in horizontal scrolling.
	typically, this element is the one that *you* will style
	the most.
*/
.items div {
	float:left;
	padding:5px;
}

.tool_button {
	height:24px;
	width:24px; 
	padding:5px; 
	cursor:pointer;

}

</style>
<script type='text/javascript' src='jquery/jquery-1.4.2.min.js'></script>
<script type='text/javascript' src='jquery/plugins/jquery.tools.min.js'></script>
<script type='text/javascript' src='js/js.functions.common.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		$(".scrollable").scrollable();
	});
	
	function submitUser() {
		if(validateFilledForm(document.user)) {
			xajax_submitUser(xajax.getFormValues("user"));
		} else alert("All fields required!");
		return false;
	}
	
	function updateUser() {
		if(validateFilledForm(document.user)) {
			xajax_updateUser(xajax.getFormValues("user"));
		} else alert("All fields required!");
		return false;
	}
	
	function checkExt(){
		var filename = document.getElementById('upload').value;
		var filelength = parseInt(filename.length) - 3;
		var fileext = filename.substring(filelength,filelength + 3);
	  
		// Check file extenstion
		if (fileext.toLowerCase() != "gif" && fileext.toLowerCase() != "jpg" && fileext.toLowerCase() != "png") {
		  alert ("You can only upload png or gif or jpg images.");
		  return false;
		} else {
		  return true;
		}
	}
	
</script>

</head>

<body style="background-color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px;">
<div class="scrollable">
    <div style="background-image:url(images/row_tool_bg.png); background-repeat:repeat-x; width:100%;text-align:right;">
        <img src="images/delete_32.png" class="tool_button" onMouseOver="this.style.backgroundColor='#000000'" onMouseOut="this.style.backgroundColor=''" title="Close" onClick="self.parent.Shadowbox.close();"/>
    </div>
    
    <div class="items">
        <div style="width:650px; height:500px;">
       		<div style="background-color:#4d4e53; color:#FFFFFF; padding:5px; width:640px;"><strong>Upload Photo</strong><input type="button" style="float:right;" value="Back" onClick="javascript:document.location.href='profile.php';" /></div>
           	<div>
            	<div style="border:10px solid #FFFFFF; width:144px; height:144px; float:left;">
                	<?php
                        //check if upload succesful then display it
                        if($ok == 1) { 
                    ?>
                    <img src="<?php echo $filename; ?>" style="width:144px; height:144px;" />
                    <?php 
                        } 
                    ?>
                </div>
                <div style="float:left;">
				
                    <form name="imgUpload" action="" method="post" enctype="multipart/form-data">
                    <input type="file" name="upload" id="upload" />
                    <input type="submit" name="xsubmit" value="Upload" onClick="return checkExt();" />
                    </form>
            	</div>
            </div>
        </div>
    </div>
</div>
</body>
</html>