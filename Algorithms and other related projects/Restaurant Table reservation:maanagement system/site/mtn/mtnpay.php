<?php require_once('../../Connections/price_config.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_mtnpay = "-1";
if (isset($_SESSION['custid'])) {
  $colname_mtnpay = $_SESSION['custid'];
}
mysql_select_db($database_price_config, $price_config);
$query_mtnpay = sprintf("SELECT * FROM tbl_order WHERE cust_id = %s", GetSQLValueString($colname_mtnpay, "int"));
$mtnpay = mysql_query($query_mtnpay, $price_config) or die(mysql_error());
$row_mtnpay = mysql_fetch_assoc($mtnpay);
$totalRows_mtnpay = mysql_num_rows($mtnpay);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>confirmation</title>
</head>

<body>
<div align="center">
<form action="sendconfirmation.php" name="mtnpay">
<table width="500" border="0">
  <tr>
    <td>Firstname:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_first_name']; ?>" type="text" name="fname" id="fname" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>Lastname:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_last_name']; ?>" type="text" name="lname" id="lname" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>Address1:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_address1']; ?>" type="text" name="address1" id="address1" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>Address2:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_address2']; ?>" type="text" name="address2" id="address2" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>Phone Number:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_phone']; ?>" type="text" name="phonenum" id="phonenum" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>Province/Region:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_state']; ?>" type="text" name="province" id="province" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>City:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_city']; ?>" type="text" name="city" id="city" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>Postal Code:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_postal_code']; ?>" type="text" name="zip" id="zip" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>Total Amount:</td>
    <td><label>
      <input value="<?php echo $row_mtnpay['od_shipping_cost']; ?>" type="text" name="amount" id="amount" readonly="true"/>
    </label></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td></td>
  </tr>
</table>
<label>
  <input type="submit" name="submit" id="submit" value="Submit" />
</label>
</form>
</div>
</body>
</html>
<?php
mysql_free_result($mtnpay);
?>
