<?php session_start();

	if(isset($_SESSION['session_user_id'])) {
		header ("location: index.php");
	} 
	
	require_once("includes/includes.db.php");
	$q = "SELECT * FROM userauth WHERE username = '$_SESSION[session_username]'";
	$r = mysql_query($q) or die(mysql_error());  
	$row_profile = mysql_fetch_assoc($r);	
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Profile</title>
<style type="text/css">
/*
	root element for the scrollable.
	when scrolling occurs this element stays still.
*/
.scrollable {

	/* required settings */
	position:relative;
	overflow:hidden;
	width: 660px;
	height:500px;
		border:10px solid #FFFFFF;
		background-color:#d0cfcb;
}

/*
	root element for scrollable items. Must be absolutely positioned
	and it should have a extremely large width to accommodate scrollable items.
	it's enough that you set width and height for the root element and
	not for this element.
*/
.scrollable .items {
	/* this cannot be too large */
	width:20000em;
	position:absolute;
	


}

/*
	a single item. must be floated in horizontal scrolling.
	typically, this element is the one that *you* will style
	the most.
*/
.items div {
	float:left;
	padding:5px;
}

.tool_button {
	height:24px;
	width:24px; 
	padding:5px; 
	cursor:pointer;

}

</style>
<script type='text/javascript' src='/jquery/jquery-1.4.2.min.js'></script>
<script type='text/javascript' src='jquery/plugins/jquery.tools.min.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		$(".scrollable").scrollable();
	});
		
</script>

</head>

<body style="background-color:#FFFFFF; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px;">
<div class="scrollable">   
   <div style="background-image:url(images/row_tool_bg.png); background-repeat:repeat-x; width:100%;text-align:right;">
    <img src="images/delete_32.png" class="tool_button" onMouseOver="this.style.backgroundColor='#000000'" onMouseOut="this.style.backgroundColor=''" title="Close" onClick="self.parent.Shadowbox.close();"/>
  </div>

   <!-- root element for the items -->
   <div class="items">
   		 <div style="width:650px; height:500px;">
         	<table style="width:100%">
                <tr><td style="background-color:#4d4e53; color:#FFFFFF; padding:5px;" colspan="2"><strong>User Profile</strong>
                    <input type="button" value="Update Profile" style="float:right; margin-left:5px;" onclick="javascript:document.location.href='modules.update_profile.php?update_user=<?php echo $_SESSION['session_username']; ?>'" />
                    <input type="button" value="Upload Photo" style="float:right;" onclick="javascript:document.location.href='modules.upload.php'" />
                </td></tr>
            	<tr>
                	<td style="vertical-align:top; width:150px;">
                    	<table style="width:100%">
                        	<tr>
                            	<td><div style="border:10px solid #FFFFFF; width:144px; height:144px;"><?php if($row_profile['image'] != "") { ?><img src="../upload/<?php echo $row_profile['image']; ?>" style="width:144px; height:144px;" /> <?php } ?></div></td>
                            </tr>
                            <tr>
                            	<td style="text-align:center"><?php echo $_SESSION['session_username']; ?></td>
                            </tr>
                         </table>
                    </td>
                  	<td style="vertical-align:top;">
                    	<table style="width:100%" cellpadding="5">
                        	<tr>
                            	<td colspan="2">
                                </td>
							</tr>
                        	<tr>
                            	<td style="width:80px;">Full Name:</td><td><?php echo $row_profile['commonname']; ?></td>
                            </tr>
                            <tr>
                            	<td style="width:80px;">UserName:</td><td><?php echo $row_profile['username']; ?></td>
                            </tr>
                            <tr>
                            
                            <tr>
                            	<td style="width:80px;">&nbsp;</td>
                                <td>&nbsp;</td>
                             </tr>
                             <tr>
                            	<td style="width:80px;">&nbsp;</td>
                                <td>&nbsp;</td>
                             </tr>
                        </table>
                    </td>
                 </tr>
                 <tr>
                 	<td colspan="2">
                    	<div style="overflow-x:hidden; overflow-y:auto; width:640px; height:200px;"></div>
                   </td>
                 </tr>
             </table>
     	 </div>
 
   </div>
   
</div>

<!-- "next page" action -->

</body>
</html>
