
<!--
	Copyright 2009 Jeremie Tisseau
	"Sliding Login Panel with jQuery 1.3.2" is distributed under the GNU General Public License version 3:
	http://www.gnu.org/licenses/gpl-3.0.html
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
 	<title>Table Reservation System</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	

	<link type="text/css" href="jquery/css/south-street/jquery-ui-1.8.custom.css" rel="stylesheet" />	
	 
    <!-- jQuery - the core -->
	<script type='text/javascript' src='jquery/jquery-1.4.2.min.js'></script>    

	<script type='text/javascript' src='js/js.functions.common.js'></script>
    
	<script language="javascript">
	jQuery(document).ready();
        function submitUser() {
            if(validateFilledForm(document.registrationForm)) {(window.location = 'reguser.php');
            } else alert("All fields required!");
            return false;
}
    </script>
		<style type="text/css">
			/*demo page css*/
			body{font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px;}
		
</style>
</head>

<body style="padding:0px; margin:0px; background:url(images/default_bg.jpg) repeat-x scroll left top #292929;">
	<center>
	<div  class="ui-widget-content ui-corner-all" style="width:300px; height:400px; margin-top:100px;">
		
		<!-- Registration Form -->
		<form  name="registrationForm" id="registrationForm"  action="reguser.php" method="post">
			<div class="ui-state-default  ui-corner-all" style="position:relative; top:0px; left:0px; padding:5px;"> 
			  <p>Table Reservation System </p>
			  <p>Client Registration</p>
        </div>
			<div style="position:relative; top:10px; left:22px; text-align:left;">Username:</div>
			<div style="position:relative; top:15px; left:0px; text-align:center;"><input  class="ui-widget-content" type="text" name="username" id="username" value="" style="width:200px; padding:5px; font-size:12px; height:25px;" /></div>
			<div style="position:relative; top:20px; left:22px; text-align:left;">Password:</div>
			<div style="position:relative; top:25px; left:0px; text-align:center;"><input  class="ui-widget-content" type="password" name="password" id="password" style="width:200px; padding:5px; font-size:12px; height:25px;" /></div>
            <div style="position:relative; top:30px; left:22px; text-align:left;">Firstname:</div>
			<div style="position:relative; top:35px; left:0px; text-align:center;"><input  class="ui-widget-content" type="text" name="firstname" id="firstname" style="width:200px; padding:5px; font-size:12px; height:25px;" /></div>
            <div style="position:relative; top:40px; left:22px; text-align:left;">Lastname:</div>
			<div style="position:relative; top:45px; left:0px; text-align:center;"><input  class="ui-widget-content" type="text" name="lastname" id="lastname" style="width:200px; padding:5px; font-size:12px; height:25px;" /></div>
			<div style="position:relative; top:50px; left:0px; text-align:center;"><input type="submit" name="submit" value="Register" class="ui-state-default ui-corner-all" style="padding:5px; width:100px;" /></div>
		</form>
		
      <p>&nbsp;</p>
</div>
	<div class="ui-accordion ui-state-error"><?php echo $_GET['mesg']; ?></div>
    </center>
</body>
</html>
