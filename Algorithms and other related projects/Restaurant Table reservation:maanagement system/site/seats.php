<?php session_start() ?>
<html>
<head>
	<title>Tickets</title>
	<style>
		* {
			font-size: 11px;
			font-family: arial;
		}
		.scrollable {

	/* required settings */
	position:relative;
	overflow:hidden;
	width: 600px;
	height:600px;
	border:10px solid #FFFFFF;
	background-color:#d0cfcb;}
	
	
/*
	a single item. must be floated in horizontal scrolling.
	typically, this element is the one that *you* will style
	the most.
*/
.items div {
	float:left;
	padding:5px;
	}

.tool_button {
	height:24px;
	width:24px; 
	padding:5px; 
	cursor:pointer;

}
}
	</style>
    <script type='text/javascript' src='../jquery/jquery-1.4.2.min.js'></script>
<script type='text/javascript' src='../jquery/plugins/jquery.tools.min.js'></script>
<script type='text/javascript' src='../js/js.functions.common.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		$(".scrollable").scrollable();
	});
	</script>
	<script>


		function reserveSeats() {
			
			var selectedList = getSelectedList('Reserve Seats');
			
			if (selectedList) {
				if (confirm('Do you want to reserve selected seat/s ' + selectedList + '?')) {
					document.forms[0].oldStatusCode.value=0;
					document.forms[0].newStatusCode.value=1;
					document.forms[0].action='bookseats.php';
					document.forms[0].submit();
				} else {
					clearSelection();
				}
			}
		}


		function cancelSeats() {
			
			var selectedList = getSelectedList('Cancel Reservation');
			
			if (selectedList) {
				if (confirm('Do you want to cancel reserved seat/s ' + selectedList + '?')) {
					document.forms[0].oldStatusCode.value=1;
					document.forms[0].newStatusCode.value=0;
					document.forms[0].action='bookseats.php';
					document.forms[0].submit();
				} else {
					clearSelection();
				}
			}
		}


		function confirmSeats() {
			
			var selectedList = getSelectedList('Confirm Reservation');
			
			if (selectedList) {
				if (confirm('Do you want to confirm reserved seat/s ' + selectedList + '?')) {
					document.forms[0].oldStatusCode.value=1;
					document.forms[0].newStatusCode.value=2;
					document.forms[0].action='bookseats.php';
					document.forms[0].submit();
				} else {
					clearSelection();
				}
			}
		}


		function getSelectedList(actionSelected) {
			
			// get selected list
			var obj = document.forms[0].elements;
			var selectedList = '';
			for (var i = 0; i < obj.length; i++) {
				if (obj[i].checked && obj[i].name == 'seats[]') {
					selectedList += obj[i].value + ', ';
				}
			}
			
			// no selection error
			if (selectedList == '') {
				alert('Please select a seat before clicking ' + actionSelected);
				return false;
			} else {
				return selectedList;
			}

		}
		
		function clearSelection() {
			var obj = document.forms[0].elements;
			for (var i = 0; i < obj.length; i++) {
				if (obj[i].checked) {
					obj[i].checked = false;
				}
			}
		}


		function refreshView() {
			clearSelection();
			document.forms[0].action='<?php echo $_SERVER['PHP_SELF']; ?>';
			document.forms[0].submit();
		}


	</script>
</head>
<body>
<div class="scrollable">   
   <div style="background-image:url(images/row_tool_bg.png); background-repeat:repeat-x; width:100%;text-align:right;">
	<a class="prev browse left"><img src="images/left_32.png" class="tool_button" title="Back" onMouseOver="this.style.backgroundColor='#000000'" onMouseOut="this.style.backgroundColor=''" /></a>
    <a class="next browse right"><img src="images/right_32.png" class="tool_button" onMouseOver="this.style.backgroundColor='#000000'" onMouseOut="this.style.backgroundColor=''" title="Next"/></a>
    <img src="images/delete_32.png" class="tool_button" onMouseOver="this.style.backgroundColor='#000000'" onMouseOut="this.style.backgroundColor=''" title="Close" onClick="self.parent.Shadowbox.close();"/>
  </div>
  <div class="items">
<table>
<tr><td>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" >
<input type="hidden" name="oldStatusCode" value=""/>
<input type="hidden" name="newStatusCode" value=""/>
  
<table width='100%' border='0'>
	<tr><td align='center'>
		<input type='button' value='Refresh View' onclick='refreshView();'/>
	</td></tr>
</table>

<tr><td width="100%" align="center">
<table width='100%' border='0'>
	<tr><td align='center'>
		<input type='button' value='Reserve Seats' onclick='reserveSeats()'/>
		&nbsp;<input type='button' value='Confirm Reservation' onclick='confirmSeats()'/>
		&nbsp;<input type='button' value='Cancel Reservation' onclick='cancelSeats()'/>
	</td></tr>
</table>
</td></tr>
<tr><td width="100%" align="center">
<table width='100%' border='0'>
	<tr><td align='center'>
		<input type='button' value='Clear Selection' onclick='clearSelection()'/></td>
	</tr>
</table>
</td></tr>
<tr><td width="100%" align="center">
<?php

 
$linkID = @ mysql_connect("localhost", "root", '') or die("Could not connect to MySQL server");
@ mysql_select_db("tickets") or die("Could not select database");
/* Create and execute query. */
$query = "SELECT * from seats order by rowId, columnId desc";
$result = mysql_query($query);
$prevRowId = null;
$seatColor = null;
$tableRow = false;
//echo $result;
echo "<table width='100%' border='0' cellpadding='10' cellspacing='3'>";
while (list($rowId, $columnId, $status, $updatedby) = mysql_fetch_row($result))
{
	if ($prevRowId != $rowId) {
		if ($rowId != 'A') {
			echo "</tr></table></td>";
			echo "\n</tr>";
		}
		$prevRowId = $rowId;
		echo "\n<tr><td align='center'><table border='1' cellpadding='8' cellspacing='8'><tr>";
	} else {
		$tableRow = false;
	}
	if ($status == 0) {
		$seatColor = "lightgreen";
	} else if ($status == 1 && $updatedby == 'user1') {
		$seatColor = "FFCC99";
	} else if ($status == 1 && $updatedby == 'user2') {
		$seatColor = "FFCCFF";
	} else if ($status == 2 && $updatedby == 'user1') {
		$seatColor = "FF9999";
	} else if ($status == 2 && $updatedby == 'user2') {
		$seatColor = "CC66FF";
	} else {
		$seatColor = "red";
	}

	echo "\n<td  bgcolor='$seatColor' align='center'>";
	echo "$rowId$columnId";
	if ($status == 0 || ($status == 1 && $updatedby == $_SESSION['session_username'])) {
		echo "<input type='checkbox' name='seats[]' value='$rowId$columnId'></checkbox>";
	}
	echo "</td>";
		if (($rowId == 'A' && $columnId == 7) 
			|| ($rowId == 'B' && $columnId == 9) 
			|| ($rowId == 'C' && $columnId == 9) 
			|| ($rowId == 'D' && $columnId == 10) 
			|| ($rowId == 'E' && $columnId == 8) 
			|| ($rowId == 'F' && $columnId == 5) 
			|| ($rowId == 'G' && $columnId == 13) 
			|| ($rowId == 'H' && $columnId == 14) 
			|| ($rowId == 'I' && $columnId == 14) 
			|| ($rowId == 'J' && $columnId == 12) 
			|| ($rowId == 'K' && $columnId == 14) 
			|| ($rowId == 'L' && $columnId == 13) 
			|| ($rowId == 'M' && $columnId == 9)) {
			// This fragment is for adding a blank cell which represent the "center aisle"
			echo "<td>&nbsp;</td>";
		}
}

echo "</tr></table></td>";
echo "</tr>";
echo "</table>";

/* Close connection to database server. */
mysql_close();
?>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td width="100%" align="center">
	<table border="1" cellspacing="8" cellpadding="8" >
		<tr>
			<td bgcolor='lightgreen'>Available</td>
			<td bgcolor='FFCC99'>Reserved user1</td>
			<td bgcolor='FF9999'>Confirmed user1</td>
			<td bgcolor='FFCCFF'>Reserved user2</td>
			<td bgcolor='CC66FF'>Confirmed user2</td>
		</tr>
	</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td width="100%" align="center">
  <a href="seats.php" target="new">View Layout</a> 
</form>
</td></tr>
</table></div></div>
</body>
</html>