##Kasiemobi Maduabunachukwu
#0990649
#ideas are gotten from the folllowing:
#http://www.cleveralgorithms.com/nature-inspired/advanced/testing_algorithms.html
#https://www.codeproject.com/Articles/3172/A-Simple-C-Genetic-Algorithm
#http://www.codemiles.com/c-examples/genetic-algorithm-example-t7548.html
#http://www.astro.cornell.edu/~cordes/A6523/stephanie.forrest.pdf
#https://www.cs.usfca.edu/~brooks/F03classes/ai/homeworks/gaproj.pdf
# One.py - useage example
#
# the fittest individual will have a chromosome consisting of 256 '1's
#
import genetic
import matplotlib


matplotlib.use('Agg')

import matplotlib.pyplot as plt


class OneMax(genetic.Individual):
    optimization = genetic.MAXIMIZE 
    def evaluate(self, optimum=None):
        self.score = sum(self.chromosome)
    def mutate(self, gene):
        self.chromosome[gene] = not self.chromosome[gene] # bit flip
    def sum(self, seq):
        def add(x,y): 
         return x+y
         return reduce(add, seq, 0)
if __name__ == "__main__":
    env = genetic.Environment(OneMax, maxgenerations=3000, optimum=256)
    env.run()
