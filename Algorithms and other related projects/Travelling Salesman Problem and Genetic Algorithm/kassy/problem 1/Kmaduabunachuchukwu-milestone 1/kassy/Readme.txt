{\rtf1\ansi\ansicpg1252\cocoartf1504\cocoasubrtf600
{\fonttbl\f0\froman\fcharset0 Times-Roman;}
{\colortbl;\red255\green255\blue255;\red255\green255\blue255;\red0\green0\blue0;\red0\green0\blue0;
\red27\green31\blue34;\red255\green255\blue255;}
{\*\expandedcolortbl;\csgray\c100000;\csgray\c100000;\csgenericrgb\c0\c0\c0;\cssrgb\c0\c0\c0;
\cssrgb\c14118\c16078\c18039;\cssrgb\c100000\c100000\c100000;}
\paperw11900\paperh16840\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\qc\partightenfactor0

\f0\b\fs24 \cf0 \ul \ulc0 Soft computing Assignment2 Milestone 1 readme file\
by\
Kasiemobi Maduabunachukwu : 0990649\
Demonstrating the performance of the genetic algorithm and checking for evaluation, validation and classification.\
\pard\pardeftab720\partightenfactor0
\cf0 \cb2 \
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\qc\partightenfactor0
\cf0 \
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0
\cf0 How to compile code written in python:
\b0 \ulnone \
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\qj\partightenfactor0
\cf0 Training the algorithm: Adjust the number the population size, mutation rate, length of bit string and set the elitism to true you would like to have in the code and run as "python One.py" on a mac/windows  by changing file path to location of file on terminal and observe output in terminal. Ensure python library, matploit is installed as well.\
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0
\cf0 Testing the algorithm: Ensure python is installed fully as well as matploit,  Then go ahead to make a duplicate of the algorithm and add a class which would hold the problem you d like to test on and then run program called  whatever you choose to name as . In my case, I keep mine as \'93One.py" and observe output in terminal as well as plot the results to  graph.\

\b \ul \
Test Framework and strategy
\b0 \ulnone \
\pard\pardeftab720\partightenfactor0
\cf0 \expnd0\expndtw0\kerning0
T\cf3 his GA basic algorithm  was implemented using python. \cf4 \cb2 A GA should have the following instance variables: population, populationSize, inputLength (the length of the bitstrings), objective (the function to be optimized), fractionRetained (the fraction that will survive until the next generation), selectionMethod, tournament size and selection and mutationRate.  We set up default values for instance variables and fill the list population with randomly generated chromosomes  and also use the gene function to represent randint(). Chromosomes are made thereafter randomly , followed by evolving of the population, intialization of elitism,  cross over ,  mutation and tournament. Reasonable values which we started with are: population size = 200, mutation rate = 0.01, cross_over rate = 0.50 and set the max generation to 3000, tuournament size was set to 0.5 and top 5 best are chosen to cross over.  The selection method used was the tournament selection. Rather than computing every chromosome\'92s relative fitness, we randomly choose two chromosomes and compare them. The high fitness wins, and gets to reproduce. To perform crossover, we conduct two tournaments and crossover the two winners. One problem with this GA implementation is that it\'92s possible for good solutions to get lost - since they only survive until the next generation via crossover, performance can be slower than expected. One way around this is elitism; keeping the best n solutions from the previous generation. Elitism can be done deterministically (rank chromosomes by fitness and keep the top n).\
\pard\pardeftab720\partightenfactor0
\cf3 \cb2 The algorithm so far has only been  applied to  the number of ones problem and to test if algorithm is correct and can be adapted for different problem , a testing framework /strategy as f was coined: \
\pard\pardeftab720\partightenfactor0

\i \cf0 Deterministic
\i0 : directly test the function in question, addressing questions such as: \cf3 does onemax add correctly? and does point_mutation behave correctly? This is as follow: \
test that the objective function behaves as expected\
test the creation of random strings\
test the approximate proportion of 1's and 0's\
test that members of the population are selected\
test that the observed changes approximate the intended \
probability\
test cloning with crossover\
test recombination with crossover\
test odd sized population\
test reproduce size mismatch\
\
\pard\pardeftab720\partightenfactor0
\cf4 \cb2 \

\b \cf3 \ul \ulc3 Values and Justification
\b0 \cf4 \ulnone \
\
\pard\pardeftab720\partightenfactor0
\cf5 \cb6 There are various parameters which control the nature of the evolution of the population in the application of this GA. A majority of these were determined empirically; I simply tried different values and ran sample sets to determine which values ostensibly produced the best results. The population size, for example, I initially tried as a relatively low value of about 25 but it did not perform well at all, however after increasing it to 200 it seemed to more quickly approach a better answer. My crossover rate is equal to 100% because I feel it's important, it's the main driving force behind the GA so not doing it seems odd to me, I also think doing it can never hurt since we are already practicing elitism we will never lose an answer, only run the risk of finding a better on through the crossover. My mutation rate is 0.01, I kept this low since that was suggested multiple times in the lecture. I then tried to reduce it to 0.099, 0.080, 0.070  and finally resorted that 0.01 was the best fit for this sort of problem. I did notice for small populations, such as the sample sets of about 10 items, a large mutation rate around 0.5 would very quickly obtain a near optimal answer. I suspect this was due to it very roughly mimicking the process of Simulated Annealing. The GA terminates when  the number of generations has exceeded the maximum of 3000 allowed, whichever comes first.\cf3 \cb2 \
\
  \
\pard\pardeftab720\partightenfactor0

\b \cf5 \cb6 Results/Deductions
\b0 \
Especially in the item set which is 200 items, I notice that my GA is quite a bit off from some of the best answers it was able to find  the optimal solution at 1466th generation on . I think I could have fixed these issues if I had dedicated a bit more time to fine tuning the algorithm. While the results are impressive for the speed with which they obtain near-optimal answers, they are not as good as they can be. Some ideas I have for improving the results are doing something like adjusting the constant values used in the fitness function, for one.  I could probably randomly flip one of the swaps bits in the mutation function to give a more random, but still minor change to the overall chromosome bit string representation. In the same vein, increasing the mutation rate could probably improve the overall results across the board.  If I were to run it again I would both increase my maximum number of generations to something like 15000 and also reduce the convergence percentage to 0.5 to try to avoid those fluke convergences earlier.\
\
\pard\pardeftab720\partightenfactor0
\cf3 \cb2     In summary  this algorithm seems to perform well for the ones  in a short time time of 32 seconds approximately and going over between 1200-1600 generations with an average fitness score of 250, best fitness was at 256 and the lowest at 160 . In the next milestone,  I\'92 ll be reporting the integer value for the bits as the population evolves from generation to generation , work on the fitness function for the third problem and another self chosen problem to apply algorithm on. This would better help to judge how applicable/refactorable  is the algorithm in respect to problems in other domains and estimate its performance, accuracy and optimisability.}