@ECHO off
cls
:start
ECHO.
ECHO Nursery Rhyme
SET /p animal1=Enter a type of animal 
SET /p animal1name=Enter a name for this animal 
SET /p animal2=Enter another type of animal 
SET /p animal2name=Enter a name for this animal 
SET /p place=Enter a location 
SET /p object=Enter an object 
SET /p object2=Enter a weapon 
SET /p emotion=Enter an emotion e.g. happy 
ECHO. 
ECHO %animal1name% the %animal1% had a huge crush on %animal2name%, the %animal2%. As a result, %animal1name% decided to give %animal2name% a %object% for valentines day. %animal1name% waited for %animal2name% at the %place%, hoping the gift would make %animal2name% %emotion%. Sadly, the whole event ended with %animal1name% being hit on the head with a %object2%. 
ECHO.
:: In order to sell products on this site, you need to be reviewed by providing a sample of 50 cards. A reviewer will test the cards for validity and then verify the new vendor to the site. For example - "XYZ provided me 50 cards and there was a good mix of classics and platinum and business cards and there was a 98 percent approval rating. So now I vouch for him to be a vendor on the site."
ECHO The date today is %DATE% and the time is %TIME%. 
PAUSE
