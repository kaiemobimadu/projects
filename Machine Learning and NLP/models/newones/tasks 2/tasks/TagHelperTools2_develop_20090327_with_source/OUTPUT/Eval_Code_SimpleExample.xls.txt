*10 cross validation for the TRAINING set

Dataset: SimpleExample.xls
Language of the dataset: English
Dimension: Code
Num. of Instances: 10
Num. of Attributes: 40
Classifier: class weka.classifiers.bayes.NaiveBayes
Classifier parameters: 
  

Naive Bayes Classifier

Class Question: Prior probability = 0.5 

64c:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
BOL_tell:  Normal Distribution. Mean = 0.4 StandardDev = 0.4899 WeightSum = 5 Precision = 1.0
COMMA:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
CONTAINS_NON_STOP:  Normal Distribution. Mean = 1 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
PERIOD:  Normal Distribution. Mean = 0.4 StandardDev = 0.4899 WeightSum = 5 Precision = 1.0
QUESTION_MARK:  Normal Distribution. Mean = 0.4 StandardDev = 0.4899 WeightSum = 5 Precision = 1.0
appli:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
appli_machin:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
bake:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
bake_good:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
blue:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
bread:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
bu:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
color:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
cours:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
earli:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
earlier:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
eat:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
eat_wheat:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
explain:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
explain_earlier:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
famili:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
famili_live:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
favorit:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
favorit_color:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
good:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
learn:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
learn_cours:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
live:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
machin:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
machin_learn:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
morn:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
prefer:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
sandwich:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
take:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
tell:  Normal Distribution. Mean = 0.4 StandardDev = 0.4899 WeightSum = 5 Precision = 1.0
wheat:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
wheat_bread:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
line_length:  Normal Distribution. Mean = 0.1846 StandardDev = 0.1341 WeightSum = 5 Precision = 0.07692307692307693


Class Statement: Prior probability = 0.5 

64c:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
BOL_tell:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
COMMA:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
CONTAINS_NON_STOP:  Normal Distribution. Mean = 1 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
PERIOD:  Normal Distribution. Mean = 1 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
QUESTION_MARK:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
appli:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
appli_machin:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
bake:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
bake_good:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
blue:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
bread:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
bu:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
color:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
cours:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
earli:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
earlier:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
eat:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
eat_wheat:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
explain:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
explain_earlier:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
famili:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
famili_live:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
favorit:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
favorit_color:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
good:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
learn:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
learn_cours:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
live:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
machin:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
machin_learn:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
morn:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
prefer:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
sandwich:  Normal Distribution. Mean = 0 StandardDev = 0.0017 WeightSum = 5 Precision = 0.01
take:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
tell:  Normal Distribution. Mean = 0 StandardDev = 0.1667 WeightSum = 5 Precision = 1.0
wheat:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
wheat_bread:  Normal Distribution. Mean = 0.2 StandardDev = 0.4 WeightSum = 5 Precision = 1.0
line_length:  Normal Distribution. Mean = 0.1385 StandardDev = 0.102 WeightSum = 5 Precision = 0.07692307692307693

=== Feature Selection Settings ===

(Tokenizer's language version: English)
Punctuation= true
Unigrams= true
Bigrams= true
POS Bigrams= false
Line length= true
Remove stopwords= true
Stem= true
Contains non-stopwords= true
Remove rare features= true
   -Freq. threshold for feature removal= 1
User Defined= false

=== Summary ===

Correctly Classified Instances           4               40      %
Incorrectly Classified Instances         6               60      %
Kappa statistic                         -0.2   
K&B Relative Info Score               -109.8295 %
K&B Information Score                   -1.0917 bits     -0.1092 bits/instance
Class complexity | order 0              11.375  bits      1.1375 bits/instance
Class complexity | scheme               69.8954 bits      6.9895 bits/instance
Complexity improvement     (Sf)        -58.5204 bits     -5.852  bits/instance
Mean absolute error                      0.6245
Root mean squared error                  0.7687
Relative absolute error                114.4906 %
Root relative squared error            140.9375 %
Total Number of Instances               10     

=== Confusion Matrix ===

 a b   <-- classified as
 4 1 | a = Question
 5 0 | b = Statement

