-----------------------------
GENERAL INSTRUCTION

System requirement:
TagHelperTools needs Java runtime version 1.5 (J2SE 5.0) or above.

For Windows user:
you may directly double-click runtht.bat or portal.bat to launch the tool.

For Mac user:
1. open a terminal, enter the TagHelperTools2 folder, and type in:
chmod +x runtht.command
chmod +x portal.command
2. then you may double-click runtht.command or portal.command to launch TagHelper

For Linux user:
1. open a terminal, enter the TagHelperTools2 folder, and type in:
chmod +x runtht.sh
chmod +x portal.sh
2. then you may run portal.sh or runtht.sh from the terminal as follows (the $
sign is the terminal prompt, do not type that when entering the command):
$ ./runtht.sh

-----------------------------
COMMAND LINE INSTRUCTION

Sample 1:
runtht -ng -lang ger -f uni -f bi -f posbi -cl [weka.classifiers.trees.J48 -C 0.25 -M 2] -pf j48 sample_data\German_best_demo.xls

Sample 2:
runtht -ng -lang ger -f uni -f bi -f posbi -cl [weka.classifiers.meta.FilteredClassifier -F \"weka.filters.supervised.attribute.AttributeSelection -E \"weka.attributeSelection.ChiSquaredAttributeEval \" -S \"weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N 20\"\" -W weka.classifiers.trees.J48 -- -C 0.25 -M 2] -pf attributeSelected sample_data\German_best_demo.xls

Sample 3 (invoking data sampling):
runtht -ng -lang ger -f uni -f bi -f posbi -sr .1,.2,.3,.4,.5,.6,.7,.8,.9 -cl [weka.classifiers.trees.J48 -C 0.25 -M 2] -pf sample sample_data\German_best_demo.xls 


Usage: 
runtht [options] <file1> [<file2> <file3>..]
Options include:

-help,-?                displays help information
-ng,--nongui            Use command line mode
-f,--feature <string {uni,bi,posbi,punc,ll,cns}>
                        Features to use
-stop,--stopwords       Remove stopwords or functional words from list of features
-stem,--stemwords       Reduce words to morphological root forms
-noev,--no_evaluate     DON'T perform any evaluation; by default TagHelperTools will perform evaluation
-mt,--minor3rd          Output the dataset in Monorthird format; by default the Weka format is used
-rr,--removerare        Remove rare features (occurring fewer number of times than a threshold)
-rt,--rarethresh <decimal integer>
                        The threshold used for removing rare features (specifying this option automatically turns on -rr)
-s,--sample             Sample input data
-sr,--sampleratio <string>
                        A list of sampling ratios to be sampled and compared (specifying this option automatically turns on -s). Use comma to separate each ratio parameter, such as: "0.3,0.5,0.7" . Note that no space is allowed between parameters.
-lang,--language <string {eng, ger, chi}>
                        The language of the dataset
-pf,--prefix <string>   Add the specified prefix string into the output files for the ease of identification.
-cl,--classifier <string>
                        Configure the Weka classifier and training parameters. Use brackets to surround the configuration. e.g. [weka.classifiers.trees.J48 -C 0.25 -M 2]

