THTDIR=`dirname $0`
cd $THTDIR
java -Xmx500M -Dfile.encoding=ISO-8859-1 -classpath .:./TagHelperTools2.jar:./lib/argparser.jar:./lib/chineseseg.jar:./lib/java_cup.jar:./lib/jxl.jar:./lib/LatoanBrill.jar:./lib/lingpipe-2.3.0.jar:./lib/maxent-2.4.0.jar:./lib/opennlp-tools-1.3.0.jar:./lib/ostermillerutils_1_06_01.jar:./lib/postagger-2006-05-21.jar:./lib/postagger-2006-05-21-source.jar:./lib/trove.jar:./lib/weka.jar edu.cmu.taghelpertools.middle_layer.Driver $*
