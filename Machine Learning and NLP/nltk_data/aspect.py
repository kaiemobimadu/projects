import pickle, nltk, json
from numpy import loadtxt
import numpy as np
from nltk.probability import FreqDist
# Lexicon based propagation
print('Lexicon-based propagation algorithm to generate aspects')
#input files
# load the Noun Phrases
nounPhrases=json.load(open('/Users/kasiemobimaduabunachukwu/nltk_data/main_dict.json','rb'))
#opinionPolarityDict = loadtxt("Users/kasiemobimaduabunachukwu/nltk_data/asd.txt", comments="#", delimiter=",", unpack=False)
opiononPolarityDict= np.loadtxt(open("/Users/kasiemobimaduabunachukwu/nltk_data/asd.txt", 'r'))
# load seed lexicon files
opinionPolarityDict=pickle.load(open('/Users/kasiemobimaduabunachukwu/nltk_data/asda.p','rb'))
seedAdjs=pickle.load(open('/Users/kasiemobimaduabunachukwu/nltk_data/asd.p','rb'))
seedAspects=pickle.load(open('/Users/kasiemobimaduabunachukwu/nltk_data/seedaspectlist.p','rb'))
# load all the reviews
allReviewsDict=pickle.load(open('/Users/kasiemobimaduabunachukwu/nltk_data/china_beijing_aloft_beijing_haidian copy.p','rb'))
print(len(seedAspects))
# list to store the expanded aspect list
newAspects=[]
# Propagation using aspect lexicon to expand opinion words
i=0
print('Number of seed opinions::',str((len(seedAdjs))))
for revId in nounPhrases:
    i+=1
    if i in np.linspace(0,2000,5) or i in 6*np.linspace(0,2000,5):
        print(i)
    for npPair in nounPhrases[revId]:
        taggedPair=nltk.pos_tag(npPair)
        nouns=[]
        adjs=[]
        for taggedWord in taggedPair:
            if 'JJ' in taggedWord[1] and taggedWord[0]:
                adjs.append(taggedWord[0])
            if 'NN' in taggedWord[1] and taggedWord[0]:
                nouns.append(taggedWord[0])
        for noun in nouns:
            if noun in seedAspects:
                for adj in adjs:
                    if adj not in seedAdjs:
                        seedAdjs.append(adj)
seedAdjs=list(set(seedAdjs))
# dump the new opinion list
pickle.dump(seedAdjs,open('seedAdjsnew.p','wb'))
# Propagation using expanded opinion lexicon to find aspects
i=0
print('Number of seed aspects:',str((len(seedAspects))))
aspectOpinionPairDict={}
for revId in nounPhrases:
   i+=1
if i in np.linspace(0,2000,5) or i in 6*np.linspace(0,2000,5):
   print(i)
for npPair in nounPhrases[revId]:
    taggedPair=nltk.pos_tag(npPair)
    nouns=[]
    adjs=[]
    for taggedWord in taggedPair:
        if 'JJ' in taggedWord[1] and taggedWord[0]:
            adjs.append(taggedWord[0])
        if 'NN' in taggedWord[1] and taggedWord[0]:
            nouns.append(taggedWord[0])
    appendList=[]
    for adj in adjs:
        for noun in nouns:
            appendList.append([adj,noun])
        if adj in seedAdjs:
            for noun in nouns:
                if noun not in seedAspects:
                    newAspects.append(noun)
    aspectOpinionPairDict[revId]=appendList
# dump new aspect list and aspect opinion pair for each review.
pickle.dump(newAspects,open('newSeedAspects.p','wb'))
pickle.dump(aspectOpinionPairDict,open('aspectOpinionPairDict.p','wb'))
print('Number of seed adjectives:',str((len(seedAdjs))))
print('Number of new aspects:',str((len(newAspects))))
# Aspect Pruning-Populate most frequent aspects from the new aspects list
print('\n\nselecting only most frequent of the aspects')
thresholdval = 98
newSeedAspects=pickle.load(open('newSeedAspects.p','rb'))
allNounsList=pickle.load(open('allNounsList.p','rb'))
fd=FreqDist()
for noun in allNounsList:
    fd[noun]+=1
i=0
topAspects=[]
for w in sorted(fd, key=fd.get, reverse=True):
   i+=1
#print(w,fd[w])
   topAspects.append(w)
   if i==thresholdval:
       break
with open('topAspects.txt','w') as file:
  for item in topAspects:
       file.write(item+',')
pickle.dump(topAspects,open('topAspects.p','wb'))
# Merge the two aspect lists and remove duplicates
print('\n\nMerge the two aspect lists and remove duplicates')
aspectsToRemove=open('AspectsToRemove.txt','r').read()
aspectsToRemove=aspectsToRemove.split(',')
combinedAspects=list(set(topAspects+seedAspects))
for item in aspectsToRemove:
    try:
        combinedAspects.remove(item)
    except:
      continue
print('Total aspects after combining and pruning:',str(len(combinedAspects)))
pickle.dump(combinedAspects,open('combinedAspects.p','wb'))
# prune aspect opinon pair - use pruned aspect list
print("\n\nPrune aspect opinion pair to remove non-usable aspects")
newAspectOpinionPairDict={}
for revID in aspectOpinionPairDict:
  listOfPairs=aspectOpinionPairDict[revID]
  appendPair=[]
for pair in listOfPairs:
        noun=pair[1]
        if noun in combinedAspects:
            appendPair.append(pair)
newAspectOpinionPairDict[revID]=appendPair
# dump the pruned result
pickle.dump(newAspectOpinionPairDict,open('newAspectOpinionPairDict.p','wb'))

