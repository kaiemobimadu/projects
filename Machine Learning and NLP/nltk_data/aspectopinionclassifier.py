import urllib.request, re, pickle, nltk
adj1 = pickle.load(open('/Users/kasiemobimaduabunachukwu/nltk_data/asda.p','rb'))
opinionPolarityDict=pickle.load(open('opinionPolarityDict.p','rb')
newPolarityDict = opinionPolarityDict
errAdj=’’
success=0
i=0
for adj in adj1:
    if adj not in newPolarityDict:
        url='http://sentic.net/api/en/concept/'+str(adj)+'/polarity/'
    elif adj in newPolarityDict and newPolarityDict[adj]=='obj':
        url='http://sentic.net/api/en/concept/'+str(adj)+'/polarity/'
    else:
        continue
    i+=1
    print(str(i),url)
    try:
        html=str(urllib.request.urlopen(url).read())
    except:
        errAdj=errAdj+','+adj
        continue
    pattern=r'float">(-{0,1}\d+\.{0,1}\d*)</polarity>'
    obj=re.search(pattern,html)
    if obj:
45
polarity=float(obj.group(1))
        if polarity>=0:
            polarity='pos'
        else:
            polarity='neg'
        newPolarityDict[adj]=polarity
        success+=1
    else:
        polarity="NA"
        errAdj=errAdj+','+adj
        print(adj, str(polarity))
# dump the new opinion-polarity list
pickle.dump(newPolarityDict,open(’newPolarityDict.p','wb'))
file=open('failedOpinionWords.txt','w')
file.write(errAdj)
file.close()
print(str(success)+'/'+str(i)+' requests successful')