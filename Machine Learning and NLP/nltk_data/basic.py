import nltk
import re


abbreviations = {'dr.': 'doctor', 'mr.': 'mister', 'bro.': 'brother', 'bro': 'brother', 'mrs.': 'mistress', 'ms.': 'miss', 'jr.': 'junior', 'sr.': 'senior',
                 'i.e.': 'for example', 'e.g.': 'for example', 'vs.': 'versus'}
terminators = ['.', '!', '?']
wrappers = ['"', "'", ')', ']', '}']

class Splitter(object):
    def __init__(self):
        self.nltk_splitter = nltk.data.load('tokenizers/punkt/english.pickle')
        self.nltk_tokenizer = nltk.tokenize.TreebankWordTokenizer()

    def split(self, text):
        fp = open("/Users/kasiemobimaduabunachukwu/nltk_data/beijing/china_beijing_beijing_century_towers")
        text = fp.read()
        """
        input format: a paragraph of text
        output format: a list of lists of words.
            e.g.: [['this', 'is', 'a', 'sentence'], ['this', 'is', 'another', 'one']]
        """
        sentences = self.nltk_splitter.tokenize(text)
        tokenized_sentences = [self.nltk_tokenizer.tokenize(sent) for sent in sentences]
        return tokenized_sentences
           
def find_sentences(paragraph):
        end = True
        sentences = []
        while end > -1:
           end = find_sentence_end(paragraph)
        if end > -1:
           sentences.append(paragraph[end:].strip())
           paragraph = paragraph[:end]
           sentences.append(paragraph)
           sentences.reverse()
           sentences = self.nltk_splitter.tokenize(text)  
           tokenized_sentences = [self.nltk_tokenizer.tokenize(sent) for sent in sentences]
        return tokenized_sentences


def find_sentence_end(paragraph):
        
        [possible_endings, contraction_locations] = [[], []]
        contractions = abbreviations.keys()
        sentence_terminators = terminators + [terminator + wrapper for wrapper in wrappers for terminator in terminators]
        for sentence_terminator in sentence_terminators:
          t_indices = list(find_all(paragraph, sentence_terminator))
          possible_endings.extend(([] if not len(t_indices) else [[i, len(sentence_terminator)] for i in t_indices]))
        for contraction in contractions:
          c_indices = list(find_all(paragraph, contraction))
          contraction_locations.extend(([] if not len(c_indices) else [i + len(contraction) for i in c_indices]))
          possible_endings = [pe for pe in possible_endings if pe[0] + pe[1] not in contraction_locations]
        if len(paragraph) in [pe[0] + pe[1] for pe in possible_endings]:
          max_end_start = max([pe[0] for pe in possible_endings])
          possible_endings = [pe for pe in possible_endings if pe[0] != max_end_start]
          possible_endings = [pe[0] + pe[1] for pe in possible_endings if sum(pe) > len(paragraph) or (sum(pe) < len(paragraph) and paragraph[sum(pe)] == ' ')]
          end = (-1 if not len(possible_endings) else max(possible_endings))
        return end

class POSTagger(object):
    def __init__(self):
        pass
        
    def pos_tag(self, sentences):
        """
        input format: list of lists of words
            e.g.: [['this', 'is', 'a', 'sentence'], ['this', 'is', 'another', 'one']]
        output format: list of lists of tagged tokens. Each tagged tokens has a
        form, a lemma, and a list of tags
            e.g: [[('this', 'this', ['DT']), ('is', 'be', ['VB']), ('a', 'a', ['DT']), ('sentence', 'sentence', ['NN'])],
                    [('this', 'this', ['DT']), ('is', 'be', ['VB']), ('another', 'another', ['DT']), ('one', 'one', ['CARD'])]]
        """

        pos = [nltk.pos_tag(sentence) for sentence in sentences]
        #adapt format
        pos = [[(word, [postag]) for (word, postag) in sentence] for sentence in pos]
        return pos

fp = open("/Users/kasiemobimaduabunachukwu/nltk_data/beijing/china_beijing_beijing_century_towers")
text = fp.read()
paragraph =fp.read()
splitter = Splitter()
postagger = POSTagger()



splitted_sentences = splitter.split(text)
#splitted_= splitter.find_sentences(text)

#print splitted_sentences

#print splitted_
pos_tagged_sentences = postagger.pos_tag(splitted_sentences)

print pos_tagged_sentences