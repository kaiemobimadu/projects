PanLex Lite
David Kamholz, preparer

Latest version: http://dev.panlex.org/db/panlex_lite.zip

Published by:
PanLex, a project of The Long Now Foundation
San Francisco, California, U.S.A.
http://panlex.org

Summary
=======

PanLex Lite is a stripped down version of the main PanLex database, and is
distributed as an SQLite 3 binary database (http://sqlite.org/). The included
database file db.sqlite contains all of PanLex’s attested translations, more
than a billion in all, among more than 10,000 language varieties.

PanLex Lite contains three tables: lv (language varieties, based on PanLex lv),
ex (expressions, based on PanLex ex), and dnx (denotations, meanings, and
sources, based on PanLex dn, mn, and ap). More information on the PanLex
database design, including these concepts and those discussed below, may be
found at http://dev.panlex.org/db-design/.

The PanLex Lite schema is as follows:

CREATE TABLE lv (
    lv integer PRIMARY KEY,
    lc text,
    vc integer,
    uid text,
    ex integer,
    tt text
);

CREATE TABLE ex (
    ex integer PRIMARY KEY,
    lv integer,
    tt text
);

CREATE TABLE dnx (
    mn integer,
    ex integer,
    ap integer,
    ui integer,
    uq integer
);

The lv table closely matches the PanLex table. The principal difference is the
addition, for convenience, of two columns: uid, containing the language
variety’s uniform identifier (e.g., 'eng-000' for English), and tt, containing
the text of the language variety’s default name expression (the ID of which is
stored in the ex column, as in PanLex).

The ex table closely matches the PanLex table. The only difference is the
exclusion of the td (degraded text) column.

The dnx table is a denormalized version of the PanLex dn table. It contains dn’s
mn (meaning ID) and ex (expression ID) columns, which together uniquely identify
denotations. The dn (denotation ID) column is excluded, since PanLex Lite has no
foreign keys that refer to it. For performance reasons, three denormalized
columns are included: ap (the meaning’s source ID), ui (the meaning’s source’s
source group ID), and uq (the meaning’s source’s estimated quality score, from
lowest of 0 to highest of 9).

The dnx table’s ap, ui, and uq columns facilitate translation quality scoring
and linkage with PanLex. The default PanLex translation quality algorithm
(http://dev.panlex.org/tr-eval/) weights individual translations by uq score,
but counts multiple translations from the same source group (ui) as a single
attestation for this purpose.

Examples
========

The following query translates the expression 'book' in English (language
variety ID 187) into French (language variety ID 211), returning all distinct
expression texts in alphabetical order:

SELECT ex2.tt
FROM dnx
JOIN ex ON (ex.ex = dnx.ex)
JOIN dnx dnx2 ON (dnx2.mn = dnx.mn)
JOIN ex ex2 ON (ex2.ex = dnx2.ex)
WHERE dnx.ex != dnx2.ex AND ex.lv = 187 AND ex.tt = 'book' AND ex2.lv = 211
GROUP BY ex2.tt
ORDER BY ex2.tt
;

The following query does the same translation as the previous one and calculates
translation quality scores according to the standard PanLex algorithm, returning
all distinct expression texts with their scores, from highest to lowest score:

SELECT s.tt, sum(s.uq) AS trq FROM (
    SELECT ex2.tt, max(dnx.uq) AS uq
    FROM dnx
    JOIN ex ON (ex.ex = dnx.ex)
    JOIN dnx dnx2 ON (dnx2.mn = dnx.mn)
    JOIN ex ex2 ON (ex2.ex = dnx2.ex)
    WHERE dnx.ex != dnx2.ex AND ex.lv = 187 AND ex.tt = 'book' AND ex2.lv = 211
    GROUP BY ex2.tt, dnx.ui
) s
GROUP BY s.tt
ORDER BY trq DESC, s.tt
;

Permissions
===========

This database is released under CC0 1.0 Universal
(https://creativecommons.org/publicdomain/zero/1.0/legalcode).

Users of this database are invited to cite the following work:

David Kamholz, Jonathan Pool, and Susan M. Colowick (2014). PanLex: Building a
Resource for Panlingual Lexical Translation. Proceedings of the Ninth
International Conference on Language Resources and Evaluation (LREC), pp.
3145–50. http://www.lrec-conf.org/proceedings/lrec2014/pdf/1029_Paper.pdf.
