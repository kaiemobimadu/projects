# coding=UTF-8
import nltk
from nltk.corpus import brown
import string
import pickle
from collections import Counter
from nltk.corpus import stopwords

import csv, codecs

# This is a fast and simple noun phrase extractor (based on NLTK) and also does keyword extractions
# Feel free to use it, just keep a link back to this post
# http://thetokenizer.com/2013/05/09/efficient-way-to-extract-the-main-topics-of-a-sentence/
# Create by Shlomi Babluki
# May, 2013


# This is our fast Part of Speech tagger
#############################################################################
brown_train = brown.tagged_sents(categories='news')
regexp_tagger = nltk.RegexpTagger(
    [(r'^-?[0-9]+(.[0-9]+)?$', 'CD'),
     (r'(-|:|;)$', ':'),
     (r'\'*$', 'MD'),
     (r'(The|the|A|a|An|an)$', 'AT'),
     (r'.*able$', 'JJ'),
     (r'^[A-Z].*$', 'NNP'),
     (r'.*ness$', 'NN'),
     (r'.*ly$', 'RB'),
     (r'.*s$', 'NNS'),
     (r'.*ing$', 'VBG'),
     (r'.*ed$', 'VBD'),
     (r'.*', 'NN')
])
unigram_tagger = nltk.UnigramTagger(brown_train, backoff=regexp_tagger)
bigram_tagger = nltk.BigramTagger(brown_train, backoff=unigram_tagger)
trigram_tagger = nltk.TrigramTagger(brown_train, backoff=bigram_tagger)
#############################################################################


# This is our semi-CFG; Extend it according to your own needs
#############################################################################
cfg = {}
cfg["NNP+NNP"] = "NNP"
cfg["NN+NN"] = "NNI"
cfg["NNI+NN"] = "NNI"
#cfg["JJ+JJ"] = "JJ"
cfg["JJ+NN"] = "NNI"
#cfg["RB+JJ"] = "JJ"
cfg["RB+JJ+NN"] = "RB"
 

 
#############################################################################


class NPExtractor(object):

   def __init__(self, sentence):
        self.sentence = sentence

    # Split the sentence into single words/tokens
   def tokenize_sentence(self, sentence):
     fp = open("china_beijing_aloft_beijing_haidian.csv")
    #coroporate image.As I have a Starwood Preferred Guest member, I was given a small gift upon-check in. It was only a couple of fridge magnets in a gift box, but nevertheless a nice gesture.My room was nice and roomy, there are tea and coffee facilities in each room and you get two complimentary bottles of water plus some toiletries by 'bliss'.The location is not great. It is at the last metro stop and you then need to take a taxi, but if you are not planning on going to see the historic sites in Beijing, then you will be ok.I chose to have some breakfast in the hotel, which was really tasty and there was a good selection of dishes. There are a couple of computers to use in the communal area, as well as a pool table. There is also a small swimming pool and a gym area.I would definitely stay in this hotel again, but only if I did not plan to travel to central Beijing, as it can take a long time. The location is ok if you plan to do a lot of shopping, as there is a big shopping centre just few minutes away from the hotel and there are plenty of eating options around, including restaurants that serve a dog meat!"
     data = fp.read()
     for row in data:
         allreviewList =[]
         allreviewList.append(row)
         row = len(allreviewList)
         tokens = nltk.word_tokenize(data)
     return tokens

    # Normalize brown corpus' tags ("NN", "NN-PL", "NNS" > "NN")
   def normalize_tags(self, tagged):
        n_tagged = []
        for t in tagged:
            if t[1] == "NP-TL" or t[1] == "NP":
                n_tagged.append((t[0], "NNP"))
                continue
            if t[1].endswith("-TL"):
                n_tagged.append((t[0], t[1][:-3]))
                continue
            if t[1].endswith("S"):
                n_tagged.append((t[0], t[1][:-1]))
                continue
            n_tagged.append((t[0], t[1]))
        return n_tagged

    # Extract the main topics from the sentence
   def extract(self):

        tokens = self.tokenize_sentence(self.sentence)
        tags = self.normalize_tags(bigram_tagger.tag(tokens))

        merge = True
        while merge:
            merge = False
            for x in range(0, len(tags) - 1):
                t1 = tags[x]
                t2 = tags[x + 1]
                key = "%s+%s" % (t1[1], t2[1])
                value = cfg.get(key, '')
                if value:
                    merge = True
                    tags.pop(x)
                    tags.pop(x)
                    match = "%s %s" % (t1[0], t2[0])
                    pos = value
                    tags.insert(x, (match, pos))
                    break

        matches = []
        for t in tags:
            if t[1] == "NNP"  or t[1] == "NN" or t[1] == "NNI":
            #if t[1] == "NNP" or t[1] == "NNI" or t[1] == "NN":
                matches.append(t[0])
        return matches


# Main method, just run "python np_extractor.py"
def main():
   fp = open("china_beijing_aloft_beijing_haidian.csv")
    #coroporate image.As I have a Starwood Preferred Guest member, I was given a small gift upon-check in. It was only a couple of fridge magnets in a gift box, but nevertheless a nice gesture.My room was nice and roomy, there are tea and coffee facilities in each room and you get two complimentary bottles of water plus some toiletries by 'bliss'.The location is not great. It is at the last metro stop and you then need to take a taxi, but if you are not planning on going to see the historic sites in Beijing, then you will be ok.I chose to have some breakfast in the hotel, which was really tasty and there was a good selection of dishes. There are a couple of computers to use in the communal area, as well as a pool table. There is also a small swimming pool and a gym area.I would definitely stay in this hotel again, but only if I did not plan to travel to central Beijing, as it can take a long time. The location is ok if you plan to do a lot of shopping, as there is a big shopping centre just few minutes away from the hotel and there are plenty of eating options around, including restaurants that serve a dog meat!"
   data = fp.read()
   for row in data:
         allreviewList =[]
         allreviewList.append(row)
         rows = len(allreviewList)
   #data = [line.strip() for line in open("/Users/kasiemobimaduabunachukwu/nltk_data/beijing/china_beijing_autumn_garden_courtyard_hotel", 'r')]
   np_extractor = NPExtractor(data)
   result= np_extractor.extract()
   
   
   return result
      #return result	
      #print result
   #tagged = nltk.pos_tag(result)
   #pickle.dump(NPExtractor,open('main_dict.p','wb'))
   #pickle.dump(NPExtractor,open('main_dict.p','wb'))
   #pickle.dump(NPExtractor,open('main_dict.p','wb'))
def get_tokens():
	with open('china_beijing_aloft_beijing_haidian.csv') as beijing:
         tokens = nltk.word_tokenize((beijing.read().lower().translate(None, string.punctuation)))
	
        
	return tokens
	
	
	
if __name__ == '__main__':
  
   #main()
  result = main()
  tokens = get_tokens()
   

	#print("tokens=%s") %(tokens)
	

#print("before: len(count) = %s") %(len(count))
#stopwords = set(stopwords.words("english"))
#def remove_stopwords('sentence', 'english'):
#useful_words = [w for w in line if not w in stopwords.words('english')] 

filtered = [w for w in result if not w in stopwords.words('english')]

tagged = nltk.pos_tag(result)

print("tagged=%s") %(tagged) 
#count = Counter(tagged)
#count = Counter(useful_words)
#print("after: len(count) = %s") %(len(count))
	
#print("most_common = %s") %(count.most_common())

#tagged = nltk.pos_tag(result)
#print("tagged=%s" ) %(tagged)
#
#print "This sentence is about: %s" % ", ".join(filtered)
#pickle.dump(NPExtractor,open('main_dict.p','wb'))

