import string

from nltk.corpus import stopwords as sw
from nltk.corpus import wordnet as wn
from nltk import wordpunct_tokenize
from nltk import WordNetLemmatizer
from nltk import sent_tokenize
from nltk import pos_tag
import nltk



from sklearn.base import BaseEstimator, TransformerMixin

tokens = []
class NLTKPreprocessor(BaseEstimator, TransformerMixin):

    def __init__(self,  punct=None,
                 lower=True, strip=True):
        self.lower      = lower
        self.strip      = strip
        self.punct      = punct or set(string.punctuation)

    def fit(self, X, y=None):
        return self

    def inverse_transform(self, X):
        return [" ".join(doc) for doc in X]

    def transform(self, X):
        return [
            list(self.tokenize(doc)) for doc in X
        ]

    def tokenize(self, document):
        # Break the document into sentences
        with open('china_beijing_aloft_beijing_haidian.csv', 'rU') as document:
		tokens = nltk.word_tokenize(csv.reader(document, delimiter=',').lower().translate(None, string.punctuation))
		print tokens
        #data = csv.reader(inputfile, delimiter=',')
        for row in tokens:
         tokens.append(row)
         rows = len(tokens)  
        # print rows  
        for sent in sent_tokenize(document):
            # Break the sentence into part of speech tagged tokens
            for token, tag in pos_tag(wordpunct_tokenize(sent)):
                # Apply preprocessing to the token
                token = token.lower() if self.lower else token
                token = token.strip() if self.strip else token
                token = token.strip('_') if self.strip else token
                token = token.strip('*') if self.strip else token
                #print token
                
                # If punctuation, ignore token and continue
                if all(char in self.punct for char in token):
                   continue
               # print token
                return tokens
if __name__ == 'main':
  tokens = tokenize(self,document)
 

#print 
	#print("tokens=%s") %(tokens)
	
	#count = Counter(tokens)
	#print("before: len(count) = %s") %(len(count))
	
	#print("after: len(count) = %s") %(len(count))
	#count = Counter(useful_words)
#print("most_common = %s") %(count.most_common(10))
tagged = nltk.pos_tag(tokens)
print("tagged=%s") %(tagged)