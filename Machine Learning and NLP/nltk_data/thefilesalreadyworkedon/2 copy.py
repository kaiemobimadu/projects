import nltk
import  pickle
import json
import string
import csv
from nltk.corpus import brown
#import Txt
#from nltk.corpus import TextReader
#from nltk.corpus.reader.plaintext import PlaintextCorpusReader
#corpus = PlaintextCorpusReader("./Users/kasiemobimaduabunachukwu/nltk_data/beijing", ".*\.txt")
from nltk.corpus import stopwords
import re
#class ExtractKW(object):
    #def __init__(self):
        #self.classname = "ExtractKW"


stopwords = set(stopwords.words("english"))
allreviewList = []
# Used for word tokenization
#tokens = nltk.regexp_tokenize(corpus, sentence_re)
brown_train = brown.tagged_sents(categories='news')
sentence_re = r"""(?x)                   # set flag to allow verbose regexps
              (?:[A-Z]\.)+           # abbreviations, e.g. U.S.A.
              |\d+(?:\.\d+)?%?       # numbers, incl. currency and percentages
              |\w+(?:[-']\w+)*       # words w/ optional internal hyphens/apostrophe
              |(?:[+/\-@&*])         # special characters with meanings
            """

# The regex pattern for chunking
grammar = r"""
NP1:
    {<RB>?<JJ.*>*<NN.*>}
    {<NN.*>*<RB>?<JJ.*>}
NP:
{<NP1>}
    {<NP1><IN><NP1>}

"""
regexp_tagger = nltk.RegexpTagger(
    [
     (r'(-|:|;)$', ':'),
     (r'\'*$', 'MD'),
    
     (r'.*able$', 'JJ'),
     (r'^[A-Z].*$', 'NNP'),
     (r'.*ness$', 'NN'),
     (r'.*ly$', 'RB'),
     (r'.*s$', 'NNS'),
     (r'.*ing$', 'VBG'),
     (r'.*ed$', 'VBD'),
     (r'.*', 'NN')
])
chunker = nltk.RegexpParser(grammar)
# Open the reviews input file
with open('china_beijing_aloft_beijing_haidian.csv', 'rU') as inputfile:
    data = csv.reader(inputfile, delimiter=',')
    for row in data:
         allreviewList.append(row)
         rows = len(allreviewList)
    #for line in inputfile: 
          
      #  allreviewList.append(line.strip().split('\r'))
   # length = len(allreviewList)
    print rows
    

   
def read_sent_block(self, stream): 
  sents = []
  with open('china_beijing_aloft_beijing_haidian.csv', 'r') as inputfile:
     for review in self. read_review_block(stream): sents.extend([sent for sent in review.sents()])
     return sents    
   
    # print tree
    # Finds NP leaf nodes of a chunk tree
def leaves(tree):
    """Finds NP (nounphrase) leaf nodes of a chunk tree."""
    for subtree in tree.subtrees(filter = lambda t: t.label()=='NN'):
        yield subtree.leaves()
        
        #print subtree.leaves()
# converts all the words to lower case
def caseChange(term):
   term = term.lower()
   return term
   
# Removing stopwords and meaningless words
def acceptable_word(term):
    accepted = bool(2 <= len(term) <= 40
        and term.lower() not in stopwords)
    return accepted
# get the phrases fom chunk trees

def get_terms(tree):
   # print "I am kasie-i am looking"
    for leaf in leaves(tree):
        terms = [ caseChange(term) for w,t in leaf if acceptable_word(w)]
        terms = [w for w in tokens if not w in stopwords.words('english')]
        yield terms
        print terms

        

 # print allreviewList[12]
#Dictionary to hold the list of all the NPs for each review
main_dict={}
for i in range(rows):
#fix this in here
 # print(i)
  acceptable = []
  finaltext = ''
 # revId=allreviewList[i][0]
  #text=allreviewList[i][1]
  revId=i    
  text=str(allreviewList[i]).strip('[\'\t\']')

  text = caseChange(text)
    #print(i)
    #revId=i    
    #text=str(allreviewList[i]).strip('[\'\t\']')
  my_list = text.split(' ')
    
  for word in my_list:
        if(acceptable_word(word)):
            acceptable.append(word)
        else:
            print(word + " not accepted")
            
  print acceptable
  finaltext = ' '.join(acceptable)
    
    
    
    #continue
    #unacceptable = acceptable_word(self,text[0])
  tokens = nltk.regexp_tokenize(finaltext, sentence_re)
  postokens = nltk.tag.pos_tag(tokens)
  unigram_tagger = nltk.UnigramTagger(brown_train, backoff=regexp_tagger)
  bigram_tagger = nltk.BigramTagger(brown_train, backoff=unigram_tagger)
  trigram_tagger = nltk.TrigramTagger(brown_train, backoff=bigram_tagger)
    #print postokens   
  main_dict.update(postokens)
    
  tree = chunker.parse(postokens)
    #print tree
        

        
              
if __name__ == '__main_dict__':
 terms = get_terms(tree)
 aspectList=[]
 for term in terms:
    if len(term)>1:
        aspectList.append(term)
        main_dict[revId]=aspectList
#print "aspectList", asmain_dict
# Dump the Nps to a file
pickle.dump(main_dict, open('main_dict.p','wb'))



