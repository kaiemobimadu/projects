
import re
import nltk
from nltk.corpus import stopwords

abbreviations = {'dr.': 'doctor', 'mr.': 'mister', 'bro.': 'brother', 'bro': 'brother', 'mrs.': 'mistress', 'ms.': 'miss', 'jr.': 'junior', 'sr.': 'senior',
                 'i.e.': 'for example', 'e.g.': 'for example', 'vs.': 'versus'}
terminators = ['.', '!', '?']
wrappers = ['"', "'", ')', ']', '}']

class Splitter(object):
     
    def __init__(self):
        self.nltk_splitter = nltk.data.load('tokenizers/punkt/english.pickle') 
        self.nltk_tokenizer = nltk.tokenize.TreebankWordTokenizer()
  
    def split(self, text):
        fp = open("/Users/kasiemobimaduabunachukwu/nltk_data/beijing/china_beijing_beijing_century_towers")
        data = fp.read()
        """
        input format: a paragraph of text
        output format: a list of lists of words.
            e.g.: [['this', 'is', 'a', 'sentence'], ['this', 'is', 'another', 'one']]
        """
        sentences = self.nltk_splitter.tokenize(data)
        tokenized_sentences = [self.nltk_tokenizer.tokenize(sent) for sent in sentences]
        return tokenized_sentences
        

    def find_sentences(self,paragraph):
        fp = open("/Users/kasiemobimaduabunachukwu/nltk_data/beijing/china_beijing_beijing_century_towers")
        data = fp.read()
        end = True
        sentences = []
        while end  < -1:
           end = self.find_sentence_end(data)
           #sentences = self.nltk_splitter.tokenize(data)
           sentences = [self.nltk_tokenizer.tokenize(data) for sent in sentences]
        if end > -1:
           sentences.append(paragraph[end:].strip())
           paragraph = paragraph[:end]
           sentences.append(paragraph)
           sentences.reverse()
           sentences = self.nltk_splitter.tokenize(data) 
           sentences = [self.nltk_tokenizer.tokenize(sent) for sent in sentences]
           return sentences
           print sentences

  #  def find_sentence_end(self, paragraph):
    #    fp = open("/Users/kasiemobimaduabunachukwu/nltk_data/beijing/china_beijing_beijing_century_towers")
     #   data = fp.read()
       # [possible_endings, contraction_locations] = [[], []]
      #  contractions = abbreviations.keys()
       # sentence_terminators = terminators + [terminator + wrapper for wrapper in wrappers for terminator in terminators]
       # for sentence_terminator in sentence_terminators:
         # t_indices = list(self.find_all(paragraph, sentence_terminator))
        #  possible_endings.extend(([] if not len(t_indices) else [[i, len(sentence_terminator)] for i in t_indices]))
      #  for contraction in contractions:
          #c_indices = list(self.find_all(paragraph, contraction))
         # contraction_locations.extend(([] if not len(c_indices) else [i + len(contraction) for i in c_indices]))
        #  possible_endings = [pe for pe in possible_endings if pe[0] + pe[1] not in contraction_locations]
      #  if len(paragraph) in [pe[0] + pe[1] for pe in possible_endings]:
       #   max_end_start = max([pe[0] for pe in possible_endings])
        #  possible_endings = [pe for pe in possible_endings if pe[0] != max_end_start]
         # possible_endings = [pe[0] + pe[1] for pe in possible_endings if sum(pe) > len(paragraph) or (sum(pe) < len(paragraph) and paragraph[sum(pe)] == ' ')]
         # end = (-1 if not len(possible_endings) else max(possible_endings))
        #return end


    def find_all(self, a_str, sub):
         start = 0
         while True:
           start = a_str.find(sub, start)
         if start == -1:
            return
         yield start
         start += len(sub)


#fp = open("/Users/kasiemobimaduabunachukwu/nltk_data/beijing/china_beijing_beijing_century_towers")
#data = fp.read()
splitter = Splitter()
splitterFindSentence = Splitter()


#splitted_sentences = splitter.split(data)
#print splitted_sentences
 
#print "======================================================="

#splitted_sentences = splitterFindSentence.find_sentences(data)
#print "\n"
#print splitted_sentences


   #print '\n"This sentence is about: %s" % ",\n' ".join(result)
# #Print '\n------\n'.join(tokenizer.tokenize(data))
