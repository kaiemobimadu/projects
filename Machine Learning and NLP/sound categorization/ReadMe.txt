
####Kasiemobi Maduabunachukwu-0990649

Download UrbanSound8K dataset from:
https://serv.cusp.nyu.edu/projects/urbansounddataset/urbansound8k.html  
(it is free, with thanks to Justin Salamon, Christopher Jacoby, and Juan Pablo
Bello for creating the UrbanSound8K dataset)

Place tarfile in feature extractions directory. 
Run each feature extraction notebook. (May take a few hours) 

Move resulting pickle files, ending in `.p`, to Models folder.
Run desired model notebooks. (Time varies)

Make sure you install:
Tensorflow v 0.12.1 is what's required. anything above may give you errors’/Anaconda is a good alternative installed as a virtualenv
Librosa
Keras
Pysound
Jupyter Notebook
Sckitlearn
Numpy
Scipy 

Make sure to run with python 3, if you use a mac and use numb 0.12.1 or else you run into endless loop. its a bug with numpy dealing with floats as integers

Your environment may ask you for a few other modules, be patient and install




