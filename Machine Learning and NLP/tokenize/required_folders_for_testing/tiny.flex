
/*
  File Name: regularexpressions.flex
  Created By: kasiemobi Maduabunachukwu
*/

package tokenize;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
%%

%public 

%class AnotherLexer

/*  %type Token  */
%type void

%line

%column

%eofval{
     return ;
%eofval}

%{ 
	//Define ArrayList objects that will hold the different kinds of tokens
	private ArrayList _normalTokens;
	private ArrayList _hyphenatedTokens;
	private ArrayList _apostrophizedTokens;
	private String _tokenFileName="";
	//Define a hashmap to hold the locations of the indexes of tokens to be replaced because they contain delimiters hyphen or apostrophe
    private Map<Integer, ArrayList> _replacementMap ;
	
	
	public String getTokenFileName()
	{
		return _tokenFileName;
	}
	
	public void setTokenFileName(String fn)
	{
	   _tokenFileName=fn;
	}
	public ArrayList getNormalTokenList()
	{
		return _normalTokens;
	}
	
	public ArrayList getHyphenatedTokenList()
	{
		return _hyphenatedTokens;
	}
	
	public ArrayList getApostrophizedTokenList()
	{
		return _apostrophizedTokens;
	}
	
	public boolean addApprostophizedToken(String token)
	{
		boolean status=false;
		
                 if (token!=null) {
                    if (!token.isEmpty()) {
                        try {
                             _apostrophizedTokens.add(token.trim().toLowerCase());
                            status = true;
                        } catch(Exception ex)
                        {
                            status=false;
                            //TODO log the exception in some way
                        }                       
                    }
                 }	
		return status;
	}
	
	public boolean addHyphenatedToken(String token)
	{
		boolean status=false;
		
                 if (token!=null) {
                    if (!token.isEmpty()) {
                        try {
                             _hyphenatedTokens.add(token.trim().toLowerCase());
                            status = true;
                        } catch(Exception ex)
                        {
                            status=false;
                            //TODO log the exception in some way
                        }                       
                    }
                 }	
		return status;
	}
	
	public boolean addNormalToken(String token)
	{
		boolean status=false;
		
                 if (token!=null) {
                    if (!token.isEmpty()) {
                        try {
                             _normalTokens.add(token.trim().toLowerCase());
                            status = true;
                        } catch(Exception ex)
                        {
                            status=false;
                            //TODO log the exception in some way
                        }                       
                    }
                 }	
		return status;
	}
	
	 public void saveTokenToFile(ArrayList l, String fullFilePath) throws IOException {
        //Save the tokens into the files
        StringBuilder builder = new StringBuilder();
        long num = l.size();
        if (num >= 0) {
            String token = "";
            for (int i = 0; i < l.size(); i++) {
                token = (String) l.get(i);
                if (i < num - 1) {
                    builder.append(token);
                    builder.append(",");
                } else {
                    builder.append(token);
                }
            }
        }
        
        //write the contents of the builder to the specified file
        if (builder.length() > 0)
        {
            try (FileWriter writer = new FileWriter(fullFilePath, true)) {
//write the content of the token
                                writer.write(builder.toString());
                                writer.write(System.lineSeparator());
                                writer.flush();
								writer.close();
            } catch (IOException ex) {
                Logger.getLogger(AnotherLexer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
	
	public void combineLists(ArrayList destinationList, ArrayList sourceList1, 
            ArrayList sourceList2)
    {
        destinationList.addAll(sourceList1);
        destinationList.addAll(sourceList2);
    }
	
	public ArrayList performPreprocess(ArrayList rawTokens, String delimiter)  {
        ArrayList proclist = new ArrayList();
        //Always create a new instance of th map
        _replacementMap = new TreeMap<>(Collections.reverseOrder());
        if (rawTokens != null)
        {
            String token = "";
            
            int num = rawTokens.size();
            if (num > 0)
            {
                boolean isToBeSplit = false;
//                ArrayList tokensThatHaveBeenSplit = null;
                ArrayList tokensToRemove = new ArrayList();
                for(int i = 0; i < num; i++)  {
                      ArrayList tokensThatHaveBeenSplit = new ArrayList();
                    token           = (String) rawTokens.get(i);
                    if (token.contains(delimiter))
                    {
                        //
                        String[] parts = splitTokenByDelimiter(token, delimiter);
                        
                        isToBeSplit  = isEligibleForSplitting(parts);
                        
                        if (isToBeSplit) {
                            
                            parts = removeNumbers(parts);
                            addPartsToSplitList(tokensThatHaveBeenSplit, parts);
                            _replacementMap.put(i, tokensThatHaveBeenSplit);
                          
                            
                        }                         
                    }
                }
                

                for (Map.Entry<Integer, ArrayList> entry : _replacementMap.entrySet()) {
                    Integer key = entry.getKey();
                    ArrayList value = (ArrayList)entry.getValue();
                       //Step 1: Remove the tokens that contain hyphen/apostrophe that 
                //        were eligible to split (greater than 3 letters) from 
                //        the main token list
                    rawTokens.remove(key.intValue());
                    
                     //Step 2: Add the portions that were split into the main token
                //        list.
                    rawTokens.addAll(key, value);
                    
                }
             
            
                
               
               
            }
        }
        proclist = rawTokens;
        return proclist;
    }
	
	   public String[] removeNumbers(String[] parts) {
       String[] newParts = null;
       ArrayList<String> l = new ArrayList<String>();
        for (String part : parts) {
            if (!isNumeric(part))
                l.add(part);
        }
        
       
      newParts= new String[l.size()];
        newParts = l.toArray(newParts);
       return newParts;
    }
	
	public String[] splitTokenByDelimiter(String token, String delimiter) {
        String[] parts = null; 
        parts =  token.split(delimiter);
        return parts;
    }
	
	public boolean isEligibleForSplitting(String[] parts) {
        boolean split = false;
        
        if (parts != null)
        {
            for (String part : parts) {
                if (part.length() > 3) {
                    split = true;
                    break;
                }                    
            }
        }
        return split;
        
    }
	
	public boolean addPartsToSplitList(ArrayList tokensToSplit, String[] parts) {
        boolean status = true;
        if (tokensToSplit == null || parts == null)
        {
            status = false;
            return status;
        }        
        tokensToSplit.addAll(Arrays.asList(parts));        
        return status;
    }
	
	 public  boolean isNumeric(String str)  
        {  
          try  
          {  
            double d = Double.parseDouble(str);  
          }  
          catch(NumberFormatException nfe)  
          {  
            return false;  
          }  
          return true;  
        }
%}

%init{
	//Instantiate this variable inside the constructor.
	this._normalTokens 			= new ArrayList();
	this._hyphenatedTokens 		= new ArrayList();
	this._apostrophizedTokens 	= new ArrayList();
	this._replacementMap = new TreeMap<>(Collections.reverseOrder());
%init}



%eof{
	//Perform actions at the end of the file.
	System.out.println("Save Each Kind Of Token Differently");
	
	
	
	_hyphenatedTokens 			= performPreprocess(_hyphenatedTokens, "-");
	_apostrophizedTokens 		= performPreprocess(_apostrophizedTokens, "'");
	
	//Combine the hyphenated and the apostrophized tokens into the normal token list.
	combineLists(_normalTokens, _hyphenatedTokens, _apostrophizedTokens);
	
        //TODO: Added the preprocess here hoping that it will be fine
        _normalTokens = performPreprocess(_normalTokens, "-");
        _normalTokens = performPreprocess(_normalTokens, "'");
	
	try {
            saveTokenToFile(_normalTokens, getTokenFileName());
        } catch (IOException ex) {
            Logger.getLogger(AnotherLexer.class.getName()).log(Level.SEVERE, null, ex);
        }
		
%eof}



digit = [0-9]

letter = [a-zA-Z]

number= [+\-]?{digit}+\.{digit}+(e[+\-]?{digit}+)?|(-?{digit})+("."{digit}+)|{digit}+

identifier = ({letter}+) ({number}+) 

word =  ({letter})({letter}|{digit}+)* |({letter}+)+{letter}|[A-Z][bcdfghj-np-tvxz]+\.|  ({number}+)("-"{letter}+)+

Hyphenated = ({word}+)+("-"{word}+) | ({word}+)("-"[^((a-zA-Z)+)])+ | ({word}+)("-"{number}+)+ | ({word}+)("-"{number})("-" {word}+)+| ({number}+)("-"{word}+)+ | ({number}+)("-"{letter}+)+
 
Apostrophized = ({word}"'"){word}+|{word} ("'"{word})+|({word}"'"{word}"'"){word}+

//LineTerminator 		= \r|\n|\r\n
//WhiteSpace     		= [\t\f\n]|{LineTerminator}
stringch 			= [��]
string 				= �{stringch}+�
//otherch 			=[�+\-�./]
//othersym 			= {otherch}+



%%
/* ------------------------Lexical Rules Section---------------
------- */

/*
   This section contains regular expressions and actions, i.e. Java
   code, that will be executed when the scanner matches the associated
   regular expression.  /* {Hyphenated}'{identifier} */
   */

{Hyphenated}        {  /* return new Token(Token.Hyphenated, yytext(), yyline); */
						/*addHyphenatedToken(yytext());*/
						addNormalToken(yytext());

}

{Apostrophized}     {  /*return new Token(Token.Apostrophized, yytext(), yyline);*/
						/*addApprostophizedToken(yytext());*/
						addNormalToken(yytext());
}

{number}            {  /*return new Token(Token.Num, yytext(), yyline); */
						addNormalToken(yytext());
}

{word}       		{ /*return new Token(Token.WORD, yytext(), yyline); */
						addNormalToken(yytext());
}

{string}         	{  /* return new Token(Token.string, yytext(), yyline); */ 
						addNormalToken(yytext());
 }

{identifier}		{ /* return new Token(Token.ID, yytext(), yyline);* /
						addNormalToken(yytext());
}

{othersym}  		{  return new Token(Token.OSym, yytext(), yyline); }

{WhiteSpace}        { /* Do Nothing */} 

\r      			{ /* Do nothing */}

\n    				{   /* Do nothing */ }

\r\n   				{ /* Do nothing */}

. 					{  /*Do nothing*/    }

