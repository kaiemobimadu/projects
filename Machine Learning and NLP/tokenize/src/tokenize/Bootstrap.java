/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokenize;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 *
 * @author kasiemobi Maduabunachukwu
 */

public class Bootstrap {

    //    private static ArrayList _tokenizedSentences = new ArrayList();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] argv) {

        //TODO: Read ALL paths, settings and options from an external file.
        //TODO: meanwhile, replace these paths with folder paths on your system.
        final boolean skipFirstLineInSentenceSegmentedfiles = true;
        final String sentenceFolderName = "/Users/kasiemobi-maduabunachukwu-pc/Downloads/tokenize/required_folders_for_testing/sample_sentences";
        final String sentenceBasedTokenFolderName = "/Users/kasiemobi-maduabunachukwu-pc/Downloads/tokenize/test";
        final String centralStatsOutputFolderName = "/Users/kasiemobi-maduabunachukwu-pc/Downloads/tokenize/required_folders_for_testing/central_stats";
        final String docLevelStatsOutputFolderName = "/Users/kasiemobi-maduabunachukwu-pc/Downloads/tokenize/required_folders_for_testing/doc_level_stats";
        final  String tokenFullFilename = "/Users/kasiemobi-maduabunachukwu-pc/Downloads/tokenize/required_folders_for_testing/sample_sentences/51664.txt";
        long numSentences = 0;
        long collectionSentenceLength  =0;
        boolean isSkipFirstLine = false;
        boolean isWordOrCharBasedCount = true; //true meaning count based on words/tokens, false meaning characters
        boolean isZeroOrOneBasedIndex = true;//meaning count from 0.
        
        System.out.println("Initializing the Statistics engine...");
        Statistics stats = new Statistics();
        stats.setSentenceFolder(sentenceFolderName);
        stats.setCentralStatsOutputFolder(centralStatsOutputFolderName);
        stats.setDocLevelOutputFolder(docLevelStatsOutputFolderName);
        stats.setTokenFolder(sentenceBasedTokenFolderName);

        numSentences = 0; 
        
        numSentences = stats.countNumSentences();
        
        collectionSentenceLength = stats.
                getCollectionSentenceLength(sentenceBasedTokenFolderName,
                                            isSkipFirstLine,
                                            isWordOrCharBasedCount,
                                            isZeroOrOneBasedIndex);

        System.out.println("Collection Level Statistics ....");
        System.out.println("\tNumber of Sentences in Collection = " + numSentences);
        System.out.println("\tTotal Sentence Length in Collection = " + collectionSentenceLength);
        System.out.println("");
        System.out.println("Document Level Statistics ....");
        System.out.println("\tDocument Level Sentence Length");
        
       
        Map<Long, Long> mapDocSentenceLens = stats.
                getDocumentSentenceLength(
                        tokenFullFilename,
                        isSkipFirstLine,
                        isWordOrCharBasedCount,
                        isZeroOrOneBasedIndex);

        if (mapDocSentenceLens.isEmpty()) {
            System.out.println("\tThere are no sentences in file: " + tokenFullFilename);
        } else {
            System.out.println("\tIn file " + tokenFullFilename + "\n\tThere are the following sentence lengths");
            System.out.println("\t");
            for (Map.Entry<Long, Long> entry : mapDocSentenceLens.entrySet()) {
                Long key = entry.getKey();
                Long value = entry.getValue();
                System.out.println("\tFor Sentence index " + key + " Sentence Length = " + value);

            }
        }
        
        //TODO: Esther, uncomment from lines 90 to 130 to run the tokenization
        AnotherLexer scanner = null;
        try {

            //Step 1. List all the input sentences in the folder.
           
            String[] sentenceArray = getSentences(sentenceFolderName);

            if (sentenceArray != null) {
                for (String sentenceFilename : sentenceArray) {
                    File f = new File(sentenceFilename);
//                    String filename = sentenceFolderName + "\\" + f.getName();
                    String filename = sentenceFolderName + File.separator + f.getName();
                    ArrayList<String> lines = new ArrayList<String>();

                    lines = getLines(filename, skipFirstLineInSentenceSegmentedfiles);

                    
                    for (String line : lines) {  
                        String tokenFilename = sentenceBasedTokenFolderName + File.separator + getFileNameFromPath(filename);
                        //String tokenFilename = getFileNameFromPath(filename);
                        File fn = new File(tokenFilename);
                        if (!fn.exists()){
                            //Create the file.
                            fn.createNewFile();
                        }
                        InputStream stream = new ByteArrayInputStream(line.getBytes(StandardCharsets.UTF_8));
                        java.io.Reader reader = new java.io.InputStreamReader(stream);
                        scanner = new AnotherLexer(reader);
                        //Set the filename for these tokens that you will get
                        scanner.setTokenFileName(tokenFilename);
                        
                        scanner.yylex();
                        
                        scanner.yyclose();
                    }

                }
            }
        } catch (Exception ex) {

        }

        return;
    }

//    public static String getFileNameFromPath(String path) {
//        if (path.contains("/"))
//            return path.substring(path.lastIndexOf('/') + 1);
//        else
//            return path.substring(path.lastIndexOf('\\') + 1);
//}
    public static String getFileNameFromPath(String path) {
        if (path.contains(File.separator)) {
            return path.substring(path.lastIndexOf(File.separatorChar) + 1);
        } else {
            return path.substring(path.lastIndexOf(File.separatorChar) + 1);
        }
    }

    public static String[] getSentences(String folderName) {
        String[] files = null;
        // create a file that is really a directory
        File aDirectory = new File(folderName);

        // get a listing of all files in the directory
        files = aDirectory.list();

        // sort the list of files (optional)
        // Arrays.sort(filesInDir);
        // have everything i need, just print it now
//    for ( int i=0; i<filesInDir.length; i++ )
//    {
//      System.out.println( "file: " + filesInDir[i] );
//    }
        return files;
    }

    public static ArrayList<String> getLines(String filename, boolean skipFirstLine) throws FileNotFoundException, IOException {
        ArrayList<String> lines = new ArrayList<String>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filename));
        } catch (Exception ex) {
            System.out.println("Exception occured reading sentence line: " + ex.getMessage());

        }
        if (reader == null) {
            return lines;
        }

        //Skip the first line
        if (skipFirstLine) {
            reader.readLine();//Read and ignore the returned value
        }
        //Continue with the other lines.
        String line1 = null;
        while ((line1 = reader.readLine()) != null) { //loop will run from 2nd line
            //some code
            line1 = line1.trim();
            if (!line1.isEmpty()) {
                lines.add(line1);
            }
        }

        return lines;
    }

//    	public static boolean addToken(String token)
//	{
//		boolean status=false;
//		
//                 if (token!=null) {
//                    if (!token.isEmpty()) {
//                        try {
//                             _tokenizedSentences.add(token.trim().toLowerCase());
//                            status = true;
//                        } catch(Exception ex)
//                        {
//                            status=false;
//                            //TODO log the exception in some way
//                        }                       
//                    }
//                 }	
//		return status;
//	}
}
