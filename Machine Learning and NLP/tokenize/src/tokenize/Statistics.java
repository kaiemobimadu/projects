/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokenize;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import static tokenize.Bootstrap.getLines;

/**
 *
 * @author kasiemobi Maduabunachukwu This class performs the following: a.
 * Central statistics b. Document level statistics
 */
public class Statistics {

    /**
     * @return the _tokenFolder
     */
    public String getTokenFolder() {
        return _tokenFolder;
    }

    /**
     * @param _tokenFolder the _tokenFolder to set
     */
    public void setTokenFolder(String _tokenFolder) {
        this._tokenFolder = _tokenFolder;
    }

    /**
     * @return the _docLevelOutputFolder
     */
    public String getDocLevelOutputFolder() {
        return _docLevelOutputFolder;
    }

    /**
     * @param _docLevelOutputFolder the _docLevelOutputFolder to set
     */
    public void setDocLevelOutputFolder(String _docLevelOutputFolder) {
        this._docLevelOutputFolder = _docLevelOutputFolder;
    }

    /**
     * @return the _sentenceFolder
     */
    public String getSentenceFolder() {
        return _sentenceFolder;
    }

    /**
     * @param _sentenceFolder the _sentenceFolder to set
     */
    public void setSentenceFolder(String _sentenceFolder) {
        this._sentenceFolder = _sentenceFolder;
    }

    /**
     * @return the _centralStatsOutputFolder
     */
    public String getCentralStatsOutputFolder() {
        return _centralStatsOutputFolder;
    }

    /**
     * @param _centralStatsOutputFolder the _centralStatsOutputFolder to set
     */
    public void setCentralStatsOutputFolder(String _centralStatsOutputFolder) {
        this._centralStatsOutputFolder = _centralStatsOutputFolder;
    }
    //Define some input parameters that this class will require.
    private String _sentenceFolder = "";
    private String _tokenFolder = "";
    private String _centralStatsOutputFolder = "";
    private String _docLevelOutputFolder = "";

    public Statistics() {
        //no argument constructor
    }

    public Statistics(String inSentenceFolder, String inTokenFolder, String outCentralStatsFolder, String outDocumentLevelStatsFolder) {
        setSentenceFolder(inSentenceFolder);
        setTokenFolder(inTokenFolder);
        setCentralStatsOutputFolder(outCentralStatsFolder);
        setDocLevelOutputFolder(outDocumentLevelStatsFolder);
    }

    /**
     * Collection level statistic.
     *
     * @return the number of sentences in the collection.
     */
    public long countNumSentences() {
        long numSentences = 0;
        String folder = getSentenceFolder();
        //get an array of files in the sentence folder
        String[] sentenceArray = Bootstrap.getSentences(folder);
        boolean skipFirstLineInSentenceSegmentedfiles = false;
        ArrayList<String> lines = new ArrayList<>();
        //Loop through each file
        if (sentenceArray != null) {
            for (String sentenceFilename : sentenceArray) {
                File f = new File(sentenceFilename);
                String filename = folder + File.separator + f.getName();

                try {
                    lines = Bootstrap.getLines(filename, skipFirstLineInSentenceSegmentedfiles);
                    if (lines != null) {
                        numSentences += lines.size();
                    }

                } catch (IOException ex) {
                    Logger.getLogger(Statistics.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return numSentences;
    }

    /**
     * Counts the total length of all sentences in a TOKENIZED document. This
     * means that (1)each sentence appears on a line. (2)each word (token) in a
     * sentence is separated by a coma from others (3) each sentence is the file
     * is separated from other sentences by a newline character.
     *
     * The count can be based on words or based on characters.
     *
     * @return a <code>Map</code> that contains as Key the sentence's 0 or
     * 1-based index and as the value the length of that sentence in either
     * characters or words(tokens)
     * @param tokenFullFilename The tokenFullFilename
     * @param isSkipFirstLine a <code>boolean</code> that if TRUE means the
     * first sentence is not counted and if FALSE, all sentences are counted.
     * @param isWordOrCharBasedCount a <code>boolean</code> that if TRUE means
     * count is based on Word and if FALSE, the count is based on characters.
     * @param isZeroOrOneBasedIndex a <code>boolean</code> that if TRUE means
     * that the sentence index is zero based and if FALSE is 1-based.
     */
    public Map<Long, Long> getDocumentSentenceLength(String tokenFullFilename, boolean isSkipFirstLine, boolean isWordOrCharBasedCount, boolean isZeroOrOneBasedIndex) {
        Map<Long, Long> mapSentenceLength = new TreeMap<>();
        ArrayList<String> lines = null;
        try {
            lines = Bootstrap.getLines(tokenFullFilename, isSkipFirstLine);

        } catch (IOException ex) {
            Logger.getLogger(Statistics.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (lines != null) {
            if (lines.size() > 0) {
                long sentenceIndex = 0;
                if (!isZeroOrOneBasedIndex) {
                    sentenceIndex = 1;
                }
                for (String line : lines) {
                    String[] tokenArr = line.split(",");
                    if (tokenArr != null) {
                        if (isWordOrCharBasedCount) //We're counting by tokens if TRUE 
                        {
                            mapSentenceLength.put(sentenceIndex++, new Long(tokenArr.length));
                        } else {
                            long tokencharlen = 0;
                            for (String token : tokenArr) {
                                tokencharlen += token.length();
                            }
                            mapSentenceLength.put(sentenceIndex++, tokencharlen);
                        }
                    }
                }
            }

        }
        return mapSentenceLength;
    }

    /**
     * Counts the total length of all sentences in a TOKENIZED document. This
     * means that (1)each sentence appears on a line. (2)each word (token) in a
     * sentence is separated by a coma from others (3) each sentence is the file
     * is separated from other sentences by a newline character.
     *
     * The count can be based on words or based on characters.
     *
     * @return a <code>Map</code> that contains as Key the sentence's 0 or
     * 1-based index and as the value the length of that sentence in either
     * characters or words(tokens)
     * @param tokenFolderName The tokenFolderName
     * @param isSkipFirstLine a <code>boolean</code> that if TRUE means the
     * first sentence is not counted and if FALSE, all sentences are counted.
     * @param isWordOrCharBasedCount a <code>boolean</code> that if TRUE means
     * count is based on Word and if FALSE, the count is based on characters.
     * @param isZeroOrOneBasedIndex a <code>boolean</code> that if TRUE means
     * that the sentence index is zero based and if FALSE is 1-based.
     */
    public long getCollectionSentenceLength(String tokenFolderName, boolean isSkipFirstLine, boolean isWordOrCharBasedCount, boolean isZeroOrOneBasedIndex) {
        long collectionSentenceLength = 0;
        TreeMap<Long, Long> mapSentenceLength = new TreeMap<>();
        String[] filesArray = Bootstrap.getSentences(tokenFolderName);

        if (filesArray != null) {
            String fn = "";
            for (String filename : filesArray) {
                File f = new File(filename);
                fn = tokenFolderName + File.separator + f.getName();
                Map<Long, Long> mapDocSentenceLength = getDocumentSentenceLength(fn, isSkipFirstLine, isWordOrCharBasedCount, isZeroOrOneBasedIndex);
                if (mapDocSentenceLength != null) {
                    for (Long value : mapDocSentenceLength.values()) {
                        collectionSentenceLength += value;
                    }
                }

            }
        }

        return collectionSentenceLength;
    }
}
