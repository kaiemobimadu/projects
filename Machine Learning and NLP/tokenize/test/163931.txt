aspirin,treatment,did,not,alter,the,rates,of,infection,or,illness,but,was,associated,with,a,moderate,reduction,in,the,frequency,or,severity,of,some,symptoms
the,overall,benefit,in,rhinovirus,infection,was,not,statistically,significant
aspirin,treatment,appeared,to,cause,a,highly,significant,increase,in,the,rate,of,virus,shedding,in,treated,subjects
the,increase,in,virus,shedding,must,be,considered,an,adverse,event,that,could,influence,the,course,of,the,disease,in,the,individual,and,increase,the,likelihood,of,the,spread,of,the,infection,to,contacts
